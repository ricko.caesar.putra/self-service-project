import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';
import SplashScreen from '../src/pages/SplashScreen';
// import HomePage from '../src/pages/HomePage';

jest.useFakeTimers();

test('SplashScreen snapShot',()=>{
    const snap = renderer.create(
        <SplashScreen/>
    ).toJSON();
    expect(snap).toMatchSnapshot();
})