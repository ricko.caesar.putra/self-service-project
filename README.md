# Moana 2.1 - TELKOMSEL Employee Self Service App

Moana 2.1 is an application developed to fulfilled all of employee needs  

### Installation

Gitlab : https://gitlab.com/kristantorobbywinner/self-service-project/-/tree/develop

Clone project and install the dependencies.

```sh
$ git clone     
$ cd 
$ npm install
$ cd ios/pod install
```

### dependencies

MyApplication uses several dependencies to support its functionality.

| Plugin | README |
| ------ | ------ |
| @@react-native-async-storage/async-storage": "github:react-native-async-storage/async-storage | -
@react-native-community/datetimepicker": "^3.0.9s | -
@react-native-community/masked-view": "^0.1.10 | -
@react-native-picker/picker": "^1.9.11 | -
@react-navigation/bottom-tabs": "^5.11.7 | -
@react-navigation/drawer": "^5.12.3 | -
@react-navigation/material-bottom-tabs": "^5.3.13 | -
axios": "^0.21.1" | -
geolib": "^3.3.1" | -
moment": "^2.29.1" | -
prop-types": "^15.7.2" | -
react": "16.13.1" | -
react-native": "0.63.4" | -
react-native-animatable": "^1.3.3" | -
react-native-async-storage": "0.0.1" | -
react-native-calendars": "^1.1249.0" | -
react-native-code-push": "^7.0.0" | -
react-native-currency-input": "^0.1.3" | -
react-native-geolocation-service": "^5.2.0" | -
react-native-gesture-handler": "^1.9.0" | -
react-native-image-crop-picker": "^0.36.0" | -
react-native-maps": "^0.27.1" | -
react-native-modal-datetime-picker": "^9.2.0" | -
react-native-paper": "^4.7.1" | -
react-native-permissions": "^3.0.0" | -
react-native-reanimated": "^1.13.2" | -
react-native-safe-area-context": "^3.1.9" | -
react-native-screens": "^2.17.1" | -
react-native-snap-carousel": "^3.9.1" | -
react-native-sound-player": "^0.10.6" | -
react-native-swiper": "^1.6.0" | -
react-native-vector-icons": "^8.0.0" | -
react-native-webview": "^11.2.1" | -
react-redux": "^7.2.2" | -
redux": "^4.0.5" | -
redux-devtools-extension": "^2.13.8" | -
redux-promise-middleware": "^6.1.2" | -
redux-thunk": "^2.3.0" | -


#### Running for IoS
```sh
$ npx react-native run-ios
```

#### Running for Android
```sh
$ npx react-native run-android
```

### Page
```
1. Splash Screen
2. Login Screen
3. Forgot Password
4. Home Screen
5. Absence
6. Absence History
7. Profile
8. Change Password
9. Leave
10. Apply Leave
11. Medical
12. Medical Info
13. Claim Medical
14. Calendar
15. Note
16. Reminder
```

### Feature
```
Moana 2.1 is an application developed to fulfilled all of employee needs  

1. Attendance - Presence Management
2. Employee Profile - Manage Leave Request
3. Medical Benefit
4. Calendar, Note & Reminder
5. Automatic update apps
6. Book Meeting Room 
7. Podcast
8. Corporate News
```

### Team C 
```
PO : Arief D Faltah
-------------------
1. Arvin Yardhika (Lead) - BE
2. Fredy Kardian - BE
3. Naufal Hariz - BE
4. Bagus Setyawan - BE
5. Emir SS Dongoran - BE
6. Diana Yurika Litik - BE
7. Ricko Caesar Putra (FE AND)
8. Kurnia Chris Pradana Wicaksono (FE AND)
9. Kristanto Robby Winner (FE IOS)
10. I Gede Ngurah Yudi Saputra (FE IOS)
```
