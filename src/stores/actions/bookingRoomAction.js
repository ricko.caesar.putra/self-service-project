import queryGetMeetingRoom from '../../Api/getMeetingRoomApi'

export default function fetchBookingRoom(token) {
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_BOOKINGROOM_REQUEST'
    });

    try {
      const result = await queryGetMeetingRoom(token);

      if (result.status === 200) {
        dispatch({
          type: 'FETCH_BOOKINGROOM_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'FETCH_BOOKINGROOM_FAILED',
          error: result.data
        });
      }
    } catch (err) {
        dispatch({
        type: 'FETCH_BOOKINGROOM_FAILED',
        error: err
      });
    }
  };
}

