import queryHomeProfile from '../../Api/homeProfileApi';

export default function fetchHomeProfile(token) {
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_HOMEPROFILE_REQUEST'
    });

    try {
      const result = await queryHomeProfile(token);

      if (result.status === 200) {
        dispatch({
          type: 'FETCH_HOMEPROFILE_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'FETCH_HOMEPROFILE_FAILED',
          error: result.data
        });
      }
    } catch (err) {
        dispatch({
        type: 'FETCH_HOMEPROFILE_FAILED',
        error: err
      });
    }
  };
}

