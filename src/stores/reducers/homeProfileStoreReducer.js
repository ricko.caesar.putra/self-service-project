const defaultState = {
    payload: {},
    isLoading: false,
    error: {}
  };
  
  export default (state = defaultState, action = {}) => {
    switch (action.type) {
      case 'FETCH_HOMEPROFILE_REQUEST': {
        return {
          ...state,
          isLoading: true
        };
      }

      case 'FETCH_HOMEPROFILE_SUCCESS': {
        return {
          ...state,
          payload: action.payload,
          isLoading: false
        };
      }
  
      case 'FETCH_HOMEPROFILE_FAILED': {
        return {
          ...state,
          payload: {},
          error: action.error,
          isLoading: false
        };
      }
      case 'LOGOUT': {
        return { ...defaultState };
      }
      default:
        return state;
    }
  };
  
  