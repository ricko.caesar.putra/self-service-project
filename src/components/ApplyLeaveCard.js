import React, { useState } from 'react';
import { StyleSheet, Text, View, Dimensions, TextInput, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import { Picker } from '@react-native-picker/picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import leave from '../Api/leaveAPI';
import { } from 'react-native-gesture-handler';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

const { width, height } = Dimensions.get('window');
const ApplyLeaveCard = ({ backPress }) => {
    const [type, setType] = useState('');
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [reason, setReason] = useState('');
    const [isDatePickerVisibleStart, setDatePickerVisibilityStart] = useState(false);
    const [isDatePickerVisibleEnd, setDatePickerVisibilityEnd] = useState(false);

    const onPressLeave = async (typeLeave, startLeave, endLeave, reasonLeave) => {
        try {
            const token = await AsyncStorage.getItem('@token');
            const startDateFormat = moment(startLeave).format('YYYY-MM-DD');
            const endDateFormat = moment(endLeave).format('YYYY-MM-DD');

            console.log(`ini datanya : ${typeLeave}, ${startLeave}, ${endLeave}, ${reasonLeave}, ${token}`);
            await leave(typeLeave, startDateFormat, endDateFormat, reasonLeave, token)
                .then((response) => {
                    console.log(response.data);
                    if (response.data.code === '200') {
                        alert(response.data.message);
                    } else {
                        alert(response.data.message);
                    }
                })
                .catch((error) => {
                    console.log(error);
                    alert(error.response.data.message);
                });
        } catch (error) {
            throw error;
        }
    };

    const showDatePickerStart = () => {
        setDatePickerVisibilityStart(true);
    };

    const showDatePickerEnd = () => {
        setDatePickerVisibilityEnd(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibilityStart(false);
        setDatePickerVisibilityEnd(false);
    };

    const handleConfirmStart = (item) => {
        console.log('A date has been picked: ', item);
        setStartDate(item);
        hideDatePicker();
    };

    const handleConfirmEnd = (item) => {
        console.log('A date has been picked: ', item);
        setEndDate(item);
        hideDatePicker();
    };

    return (
        <TouchableWithoutFeedback onPress={backPress}>
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.buttonBack}
                    onPress={backPress}>
                    <Icon name="arrow-back-outline" size={40} color={'#FFFFFF'} />
                </TouchableOpacity>
                <TouchableWithoutFeedback onPress={() => { }}>
                    <View style={styles.wrapper}>
                        <Text style={{ fontSize: 20, marginTop: 20 }}>Submit For Leave</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                            <Text style={{ flex: 0.3 }}>Type Leave</Text>
                            <View style={styles.inputType}>
                                <Picker
                                    selectedValue={type}
                                    style={styles.inputType}
                                    textStyle={{ fontSize: 12, color: 'yellow' }}
                                    onValueChange={(itemValue, itemIndex) => {
                                        setType(itemValue);
                                        console.log(itemValue);
                                    }
                                    }
                                    itemStyle={{ fontSize: 15 }}
                                >
                                    <Picker.Item label="Pilih Jenis Izin..." value={''} />
                                    <Picker.Item label="Cuti Tahunan Permanen" value={'Cuti Tahunan Permanen'} />
                                    <Picker.Item label="Ijin Karena Sakit" value={'Ijin karena Sakit'} />
                                    <Picker.Item label="Ijin karena Keperluan Pribadi" value={'Ijin karena Keperluan Pribadi'} />
                                    <Picker.Item label="Ijin Keluarga Meninggal" value={'Ijin Keluarga Meninggal'} />
                                    <Picker.Item label="Ijin Kerabat Serumah Meninggal" value={'Ijin Kerabat Serumah Meninggal'} />
                                    <Picker.Item label="Ijin Pernikahan Karyawan" value={'Ijin Pernikahan Karyawan'} />
                                </Picker>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                            <Text style={{ flex: 0.3 }}>Start Date</Text>
                            <TouchableOpacity
                                style={styles.inputDate}
                                onPress={showDatePickerStart}
                            >
                                <Text style={{ marginLeft: 8 }}>{moment(startDate).format('YYYY-MM-DD')}</Text>
                                <DateTimePickerModal
                                    isVisible={isDatePickerVisibleStart}
                                    mode="date"
                                    onConfirm={handleConfirmStart}
                                    onCancel={hideDatePicker}
                                />
                                <Icon style={{ marginRight: 11 }} name="calendar" onPress={showDatePickerStart} size={25} color={'#696969'} />
                            </TouchableOpacity>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                            <Text style={{ flex: 0.3 }}>End Date</Text>
                            <TouchableOpacity
                                style={styles.inputDate}
                                onPress={showDatePickerEnd}
                            >
                                <Text style={{ marginLeft: 8 }}>{moment(endDate).format('YYYY-MM-DD')}</Text>
                                <DateTimePickerModal
                                    isVisible={isDatePickerVisibleEnd}
                                    mode="date"
                                    onConfirm={handleConfirmEnd}
                                    onCancel={hideDatePicker}
                                />
                                <Icon style={{ marginRight: 11 }} name="calendar" onPress={showDatePickerEnd} size={25} color={'#696969'} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                            <Text style={{ flex: 0.3 }}>Reason</Text>
                            <View style={styles.input}>
                                <TextInput
                                    style={{ marginLeft: 4 }}
                                    placeholder={'Input here'}
                                    placeholderTextColor="#979797"
                                    value={reason}
                                    onChangeText={(value) => {
                                        setReason(value);
                                        console.log(reason);
                                    }}
                                />
                            </View>
                        </View>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => {
                                console.log('Submit');
                                onPressLeave(type, startDate, endDate, reason);
                            }}>
                            <Text style={{ textAlign: 'center', color: '#FFFFFF', fontSize: 15 }}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </TouchableWithoutFeedback>
    );
};

export default ApplyLeaveCard;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#00000099',
        width: width,
        height: height,
        alignItems: 'center',
        justifyContent: 'center'
    },
    wrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        width: width - 40,
        backgroundColor: '#FFFFFF',
        borderRadius: 30
    },
    input: {
        flex: 0.55,
        paddingHorizontal: 15,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'center'
    },
    inputType: {
        overflow: 'hidden',
        flex: 0.55,
        paddingHorizontal: 15,
        width: '100%',
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'center'
    },
    inputDate: {
        flex: 0.55,
        flexDirection: 'row',
        paddingHorizontal: 15,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    button: {
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: '#000000',
        borderRadius: 25,
        height: 50,
        width: 250,
        marginTop: 20,
        marginBottom: 40
    },
    buttonBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: 50,
        height: 50,
        backgroundColor: '#000000',
        borderRadius: 25,
        left: 20,
        top: 20
    }
});
