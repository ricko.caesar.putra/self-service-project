import React, { useState } from 'react';
import {
    StyleSheet,
    TouchableWithoutFeedback,
    View,
    Text,
    TextInput,
    Platform,
    TouchableOpacity
} from 'react-native';
import { colors } from '../utils';
import Icon from 'react-native-vector-icons/Ionicons';
import DateTimePicker from '@react-native-community/datetimepicker';

const AddFamilyRelationCard = ({ backPress }) => {
    const [name, setName] = useState('');
    const [dateOfBirth, setDateOfBirth] = useState(new Date());
    const [isShown, setIsShown] = useState(false);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || newDateOfBirth;
        setIsShown(Platform.OS === 'ios');
        setDateOfBirth(currentDate);
    };

    return (
        <TouchableWithoutFeedback onPress={backPress}>
            <View style={styles.container}>
                <View style={styles.wrapper}>
                    <View style={styles.formContainer}>
                        <Text style={styles.formTitle}>Name</Text>
                        <TextInput
                            placeholder="Full Name"
                            value={name}
                            autoCorrect={false}
                            multiline={true}
                            onChangeText={(value) => setName(value)}
                            style={styles.form} />
                    </View>

                    <Text style={styles.formTitle}>
                        Date Of Birth
                    </Text>
                    <TouchableOpacity style={styles.dateContainer}
                        onPress={()=>{
                            //setIsShown(true)
                            }}>

                        <Icon name={'ios-calendar'} style={{ flex: 1 }} size={20} color={colors.abuabutua} />
                        {isShown ?
                        <DateTimePicker
                            style={{
                                flex: 6,
                                padding: 10,
                                overflow: 'hidden',
                                height: 40,
                                marginLeft: 10
                            }}
                            testID="test"
                            value={dateOfBirth}
                            mode="date"
                            is24Hour={true}
                            display="default"
                            onChange={onChange}/> : null}
                    </TouchableOpacity>
                </View>
            </View>



        </TouchableWithoutFeedback>
    );
};

export default AddFamilyRelationCard;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: '#000000bc',
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
    wrapper: {
        borderRadius: 20,
        padding: 20,
        width: '80%',
        alignItems: 'center',
        backgroundColor: colors.abuabumuda
    },
    formContainer: {
        marginTop: 20,
        padding: 2,
        width: '100%',
        justifyContent: 'center'
    },
    formTitle: {
        paddingTop: 10,
        paddingBottom: 10
    },
    form: {
        padding: 5,
        width: '100%',
        height: 40,
        borderRadius: 20,
        backgroundColor: colors.putih
    },
    dateContainer: {
        width: '100%',
        marginTop: 20,
        flexDirection: 'row-reverse',
        alignItems: 'center',
        borderRadius: 25,
        backgroundColor: colors.putih
    }
});
