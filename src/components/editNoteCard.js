/* eslint-disable no-alert */
import React, { useState } from 'react';
import { Dimensions, StyleSheet, Text, TouchableOpacity, View, TouchableWithoutFeedback } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { colors } from '../utils/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import { Button } from '../components/atoms/';
import AsyncStorage from '@react-native-async-storage/async-storage';
import editNote from '../Api/editNoteApi';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

const { width, height } = Dimensions.get('screen');

const EditNoteCard = (props) => {
    const [description, setDescription] = useState(props.data.description);
    const [startDate, setStartDate] = useState(props.data.date);
    const [show, setShow] = useState(false);

    const sendEditNote = async (token, date, description, id) => {
        try {
            await editNote(token, date, description, id)
                .then(async (response) => {
                    // console.warn(response);
                    if (response.data == null) {
                        alert('submit failed');
                    } else {
                        alert('submit berhasil');
                    }
                });
        } catch (error) {
            alert(error);
        }
    };

    const showDatePicker = () => {
        setShow(true);
    };

    const hideDatePicker = () => {
        setShow(false);

    };

    const handleConfirm = (item) => {
        setStartDate(moment(item).format('YYYY-MM-DD'));
        hideDatePicker();
    };

    return (
        <TouchableOpacity
            style={styles.container}
            onPress={props.backPress}
        >
            <TouchableOpacity
                style={styles.buttonBack}
                onPress={props.backPress}>
                <Icon name="arrow-back-outline" size={40} color={'#FFFFFF'} />
            </TouchableOpacity>
            <TouchableWithoutFeedback onPress={() => null}>
                <View style={styles.wrapperBooking}>
                    <Text style={styles.text}>Description</Text>
                    <View style={styles.input2}>
                        <TextInput
                            onChangeText={(value) => setDescription(value)}
                            value={description}
                            placeholder={'Write your needs'}
                            placeholderTextColor="#979797"
                            textAlignVertical="top"
                            multiline={true}
                            onSubmitEditing={() => {
                                null;
                            }}
                        />
                    </View>
                    <Text style={styles.text}>Start :</Text>

                    <TouchableOpacity
                        style={styles.inputDate}
                        onPress={showDatePicker}
                    >
                        <Text style={{ marginLeft: 1 }} onPress={showDatePicker}>{moment(startDate).format('YYYY-MM-DD')}</Text>
                        <DateTimePickerModal
                            isVisible={show}
                            mode="date"
                            onConfirm={handleConfirm}
                            onCancel={hideDatePicker}
                        />
                        <Icon style={{ marginLeft: 1 }} name="calendar" onPress={showDatePicker} size={25} color={'#696969'} />
                    </TouchableOpacity>
                    <Button
                        title={'Edit Note'}
                        onPress={async () => {
                            const token = await AsyncStorage.getItem('@token');
                            await sendEditNote(token, moment(startDate).format('YYYY-MM-DD'), description, props.data.id);
                            props.backPress;
                        }}
                        type={'default'}
                        white={false}
                    />
                </View>
            </TouchableWithoutFeedback>
        </TouchableOpacity>
    );
};

export default EditNoteCard;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#AAAAAAdd',
        width: width,
        height: height,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 10
    },
    buttonBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: 50,
        height: 50,
        backgroundColor: '#000000',
        borderRadius: 25,
        left: 20,
        top: 20
    },
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    text: {
        fontWeight: 'bold',
        fontSize: 17,
        textAlign: 'center'
    },
    input2: {
        paddingHorizontal: 10,
        margin: 14,
        width: width * 0.9,
        height: 120,
        borderRadius: 10,
        backgroundColor: '#dddddd'
    },
    inputDate: {
        marginVertical: 14,
        width: width * 0.4,
        paddingHorizontal: 10,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    inputTime: {
        margin: 14,
        width: width * 0.4,
        paddingHorizontal: 15,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'center'
    },
    inputFasilitas: {
        width: width * 0.5,
        marginVertical: 14,
        marginRight: 10,
        padding: 5,
        height: 180,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        alignItems: 'flex-start'
    },
    wrapperBooking: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'space-between',
        margin: 10,
        borderWidth: 4,
        borderColor: colors.merah,
        height: height * 0.45,
        backgroundColor: 'white',
        borderRadius: 30,
        paddingVertical: 20
    }
});
