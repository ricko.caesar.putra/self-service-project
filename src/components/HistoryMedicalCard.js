import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import moment from 'moment';


const currencyFormat = amount => {
    return Number(amount)
        .toFixed(2)
        .replace(/\d(?=(\d{3})+\.)/g, '$&,');
};

const HistoryMedicalCard = ({ dateClaim, typeClaim, amountClaim }) => {
    return (
        <View style={styles.wrapper}>
            <View style={{ flex: 0.5, flexDirection: 'column' }}>
                <Text style={{ fontWeight: 'bold', color: '#000000', fontSize: 20 }}>{moment(dateClaim).format('DD MMM YYYY')}</Text>
                <Text style={{ fontWeight: 'bold', color: '#A82929', fontSize: 15 }}>{typeClaim}</Text>
            </View>
            <View style={{ flex: 0.5, justifyContent: 'center', alignItems: 'flex-end' }}>
                <Text style={{ fontWeight: 'bold', color: '#A82929', fontSize: 17 }}>Rp. {currencyFormat(amountClaim)}</Text>
            </View>
        </View>
    );
};

export default HistoryMedicalCard;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 25,
        marginHorizontal: 20,
        marginVertical: 3,
        backgroundColor: '#FFFFFF',
        paddingVertical: '5%',
        paddingHorizontal: '7%'

    }
});
