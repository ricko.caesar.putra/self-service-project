import React, { useState } from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import CurrencyInput from 'react-native-currency-input';
import { Picker } from '@react-native-picker/picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import claim from '../Api/claimAPI';
import DateTimePickerModal from 'react-native-modal-datetime-picker';



const { width, height } = Dimensions.get('window');
const ClaimCard = ({ backPress }) => {
    const [date, setDate] = useState(new Date());
    const [amount, setAmount] = useState();
    const [type, setType] = useState('');
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const onPressClaim = async (typeClaim, dateClaim, amountClaim) => {
        try {
            const token = await AsyncStorage.getItem('@token');
            const dateFormat = moment(dateClaim).format('YYYY-MM-DD');

            console.log(`ini datanya : ${typeClaim}, ${dateFormat}, ${amountClaim}, ${token}`);
            await claim(typeClaim, dateFormat, amountClaim, token)
                .then((response) => {
                    console.log(response);
                    if (response.data.code === 200) {
                        alert(response.data.message);
                    } else {
                        alert(response.data.message);
                    }
                })
                .catch((error) => {
                    console.log(error);
                    alert('Sisa Plafond Tidak Cukup, Silahkan Hubungi HCM');
                });
                backPress();
        } catch (error) {
            throw error;
        }
    };

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (item) => {
        console.log('A date has been picked: ', item);
        setDate(item);
        hideDatePicker();
    };



    return (
        <TouchableWithoutFeedback onPress={backPress}>
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.buttonBack}
                    onPress={backPress}>
                    <Icon name="arrow-back-outline" size={40} color={'#FFFFFF'} />
                </TouchableOpacity>
                <TouchableWithoutFeedback onPress={() => { }}>
                    <View style={styles.wrapper}>
                        <Text style={{ fontSize: 20, marginTop: 20 }}>Form Claim</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                            <Text style={{ flex: 0.3 }}>Medical Type</Text>
                            <View style={styles.inputType}>
                                <Picker
                                    style={styles.inputType}
                                    selectedValue={type}
                                    textStyle={{ fontSize: 12 }}
                                    onValueChange={(itemValue, itemIndex) => {
                                        setType(itemValue);
                                        console.log(itemValue);
                                    }}
                                    itemStyle={{ fontSize: 15 }}
                                >
                                    <Picker.Item label="Pilih Jenis Medical..." value={''} />
                                    <Picker.Item label="Rawat Inap" value={1} />
                                    <Picker.Item label="Rawat Jalan" value={2} />
                                    <Picker.Item label="Rawat Gigi" value={3} />
                                    <Picker.Item label="Persalinan" value={4} />
                                    <Picker.Item label="Kacamata" value={5} />
                                </Picker>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                            <Text style={{ flex: 0.3 }}>Date</Text>
                            <TouchableOpacity
                                style={styles.inputDate}
                                onPress={showDatePicker}
                            >
                                <Text style={{ marginLeft: 8 }}>{moment(date).format('YYYY-MM-DD')}</Text>
                                <DateTimePickerModal
                                    isVisible={isDatePickerVisible}
                                    mode="date"
                                    onConfirm={handleConfirm}
                                    onCancel={hideDatePicker}
                                />
                                <Icon style={{ marginRight: 11 }} name="calendar" onPress={showDatePicker} size={25} color={'#696969'} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                            <Text style={{ flex: 0.3 }}>Amount</Text>
                            <View style={styles.inputAmount}>
                                <CurrencyInput
                                    style={{ marginLeft: 4 }}
                                    value={amount}
                                    onChangeValue={setAmount}
                                    unit="Rp. "
                                    delimiter=","
                                    separator="."
                                    ignoreNegative={true}
                                    placeholder={'Rp. 10.000.000'}

                                    precision={0}
                                    onChangeText={(formattedValue) => {
                                        console.log(amount); // $2,310.46
                                    }}
                                />
                            </View>
                        </View>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={async () => {
                                console.log('Claim');
                                await onPressClaim(type, date, amount);
                            }}>
                            <Text style={{ textAlign: 'center', color: '#FFFFFF', fontSize: 15 }}>Claim</Text>
                        </TouchableOpacity>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </TouchableWithoutFeedback>
    );
};

export default ClaimCard;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#00000099',
        width: width,
        height: height,
        alignItems: 'center',
        justifyContent: 'center'
    },
    wrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        width: width - 40,
        backgroundColor: '#FFFFFF',
        borderRadius: 30
    },
    inputAmount: {
        flex: 0.55,
        paddingHorizontal: 15,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'center'
    },
    inputType: {
        overflow: 'hidden',
        flex: 0.55,
        paddingHorizontal: 15,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'center'
    },
    inputDate: {
        flex: 0.55,
        flexDirection: 'row',
        paddingHorizontal: 15,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    button: {
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: '#000000',
        borderRadius: 25,
        height: 50,
        width: 250,
        marginTop: 20,
        marginBottom: 40
    },
    buttonBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: 50,
        height: 50,
        backgroundColor: '#000000',
        borderRadius: 25,
        left: 20,
        top: 20
    }
});
