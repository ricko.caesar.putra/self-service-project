import axios from 'axios';

export default async function queryBookMeetingRoom(token,room_id,time,description,is_projector,is_hdmi,is_board,is_consumption,consumption) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/meeting/book';
        const headers = {
            'api_key': '76f8a1fab09bc13f2e48be45689dd074',
            'Authorization': `Bearer ${token}`
        };

        // console.warn(`inner ${type}, ${date}, ${location}, ${token}`)

        const data =
        {
            'room_id': room_id, //1
            'time': time, //"2021-02-19 15:00:00",
            'description': description, //"meeting",
            'is_projector': is_projector, //true,
            'is_hdmi': is_hdmi, //true,
            'is_board': is_board, //true,
            'is_consumption': is_consumption, //true,
            'consumption': consumption //10
        };


        // console.warn(token)
        // console.warn(data)
        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data
        });

        // console.warn(`result ${result}`)

        return (result);

    } catch (error) {
        throw error;
    }
}
