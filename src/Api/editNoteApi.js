import axios from 'axios';

export default async function editNote(token,date,description,id) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/calendar/notes/edit';
        const headers = {
            'api_key': '76f8a1fab09bc13f2e48be45689dd074',
            'Authorization': `Bearer ${token}`
        };

        // console.warn(`inner ${type}, ${date}, ${location}, ${token}`)

        const data =
        {
            'id': id, //8,
            'date': date,//"2021-03-04 13:20:00",
            'description': description //"meeting 3"
        };


        // console.warn(token)
        // console.warn(data)
        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data
        });

        // console.warn(`result ${result}`)

        return (result);

    } catch (error) {
        throw error;
    }
}
