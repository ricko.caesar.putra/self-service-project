import axios from 'axios';

export default async function checkVersion(version, OS) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/version/check';
        const headers = {
            'api_key': '76f8a1fab09bc13f2e48be45689dd074',
        };

        const data = {
            "current_version": version,
            "operating_system": OS
        };


        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data
        });

        return result;

    } catch (error) {
        throw error;
    }
}