import axios from 'axios';

export default async function queryHistoryMedical(token){
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/insurance/allinfo';
        const headers = {
            'API_KEY': '76f8a1fab09bc13f2e48be45689dd074',
            'Authorization': `Bearer ${token}`
        };

        const data = {
            'year': 2021
        };
        console.log(token);
        console.log(data);

        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data
        });
        console.log(result);
        return (result);

    } catch (error) {
        throw error;
    }
}
