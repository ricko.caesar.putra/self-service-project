import axios from 'axios';

export default async function addFamilyRelation(token, name, relationStatus, dateOfBirth, address) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/relation/add';

        const headers = {
            'api_key': '76f8a1fab09bc13f2e48be45689dd074',
            'Authorization': `Bearer ${token}`
        };

        const data = {
            'name': name,
            'relation_status': relationStatus,
            'dob': dateOfBirth,
            'address': address
        };

        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data
        });

        return result;
    } catch (error) {
        throw error;
    }
}
