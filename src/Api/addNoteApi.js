import axios from 'axios';

export default async function addNote(token,date,description) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/calendar/notes/insert';
        const headers = {
            'api_key': '76f8a1fab09bc13f2e48be45689dd074',
            'Authorization': `Bearer ${token}`
        };

        // console.warn(`inner ${type}, ${date}, ${location}, ${token}`)

        const data =
        {
            'date': date,//"2021-03-04 13:20:00",
            'description': description //"meeting 3"
        };

        // console.warn(token)
        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data
        });


        return (result);

    } catch (error) {
        throw error;
    }
}
