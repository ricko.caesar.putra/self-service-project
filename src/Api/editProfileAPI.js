import axios from 'axios'

export default async function editProfile(token, familyIdentifier, address) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/profile/edit'

        const headers = {
            'api_key': `76f8a1fab09bc13f2e48be45689dd074`,
            'Authorization': `Bearer ${token}`,
        }

        const data = {
            'address': address,
            'no_kk': familyIdentifier,
        }

        const result = await axios({
            method: 'PUT',
            url: baseUrl,
            headers,
            data,
        })

        return result
        
    } catch (error) {
        throw error
    }
}