import axios from 'axios';

export default async function leave(type_approval, start_date, end_date, reason, token) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/leave/insertLeave';
        const headers = {
            'API_KEY': '76f8a1fab09bc13f2e48be45689dd074',
            'Authorization': `Bearer ${token}`
        };

        // console.log(`isi data ${type_approval}, ${start_date}, ${end_date}, ${duration}, ${token}`);

        const data = {
            type_approval: type_approval,
            start_date: '' + start_date,
            end_date: '' + end_date,
            reason: reason
        };
        console.log(data);

        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data
        });

        console.log(`result ${result}`);

        return result;
    } catch (error) {
        throw error;
    }
}
