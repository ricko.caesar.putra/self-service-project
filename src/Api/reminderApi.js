import axios from 'axios';

export default async function getReminder(token) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/calendar/reminder';
        const headers = {
            'api_key': '76f8a1fab09bc13f2e48be45689dd074',
            'Authorization': `Bearer ${token}`,
        };

        const result = await axios({
            method: 'GET',
            url: baseUrl,
            headers,
        });

        return result;

    } catch (error) {
        throw error;
    }
}
