import React, { useEffect, useState, useCallback } from 'react'
import { StyleSheet, Text, View,Image, ScrollView, TouchableOpacity } from 'react-native'
import { color } from 'react-native-reanimated'
import { bounce } from 'react-native/Libraries/Animated/src/Easing'
import { colors } from '../../utils'
import ChangeAddressCard from '../../components/ChangeAddressCard'
import ChangeFamilyIDCard from '../../components/ChangeFamilyIDCard'
import PasswordCard from '../../components/PasswordCard'
import ImagePicker from 'react-native-image-crop-picker'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Icon from 'react-native-vector-icons/Ionicons';
import uploadAvatar from '../../Api/uploadAvatarApi'
import checkPasswordExpiry from '../../Api/checkPasswordExpiryApi'

import {connect} from 'react-redux'
import fetchUserProfile from '../../stores/actions/userAction'
import { Button } from '../../components/atoms'
import moment from 'moment'


const ProfilePage = (props) => {
    /*----- States of Forms Shown -----*/
    const [avatarIcon, setAvatarIcon] = useState(props.userStore.payload.avatar ? props.userStore.payload.avatar : Image.resolveAssetSource(require('../../assets/images/avatar-user.png')).uri)
    const [fullName, setFullName] = useState('')
    const [jobTitle, setJobTitle] = useState('')
    const [emailAddress, setEmailAddress] = useState('')
    const [userName, setUserName] = useState('')
    const [nik, setNik] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')
    const [homeBase, setHomeBase] = useState('')
    const [dateOfBirth,setDateOfBirth] = useState('')
    const [nationalIdentifier, setNationalIdentifier] = useState('')
    const [address, setAddress] = useState('')
    const [familyIdentifier, setFamilyIdentifier] = useState('')
    const [fatherName, setFatherName] = useState('')
    const [fatherAddress, setFatherAddress] = useState('')
    const [motherName, setMotherName] = useState('')
    const [motherAddress, setMotherAddress] = useState('')
    const [spouseName, setSpouseName] = useState('')
    const [spouseBirth, setSpouseBirth] = useState('')
    const [firstChildName, setFirstChildName] = useState('')
    const [firsthChildBirth, setFirstChildBirth] = useState('')
    const [secondChildName, setSecondChildName] = useState('')
    const [secondChildBirth, setSecondChildBirth] = useState('')
    
    /*----- Card Boolean States -----*/
    const [isPasswordCardPressed, setIsPasswordCardPressed] =  useState(false)
    const [isFamilyCardPressed, setIsFamilyCardPressed] = useState(false)
    const [isAddressCardPressed, setIsAddressCardPressed] = useState(false)
    const [passwordExpiryCount, setPasswordExpiryCount] = useState(0)

    /*----- Needed States -----*/
    const [imageHeadersState, setImageHeadersState] = useState('')
    
    /*----- Components Did Mount -----*/
    useEffect(()=> {
        getExpiryCounter()
        setImageHeaders1()
    },[])

    /*----- Components Did Update (User Store) -----*/
    useEffect(()=> {
        setState(props.userStore.payload)
    },[props.userStore.payload])

    const getExpiryCounter = async () => {
        try {
            const token = await AsyncStorage.getItem('@token')
            
            await checkPasswordExpiry(token)
                .then((response)=> {
                    if(response.status === 200) {
                        setPasswordExpiryCount(0)
                    } else if (response.status === 203){
                        setPasswordExpiryCount(response.data.data)
                    }
                    
                })
        } catch (error) {
            throw error
        }
    }

    const showAvatar = async () => {
        const token = await AsyncStorage.getItem('@token')
    }

    const chooseAvatarImage = async () => {
        await ImagePicker.openPicker({
            width: 80,
            height: 80,
            cropping: true,
            cropperCircleOverlay: true,
            includeBase64: true,
        }).then(async (image)=> {
            await onFinishPickAvatar(image.data)
        })
    }

    const setImageHeaders = async (url) => {
        try {
            await AsyncStorage.getItem('@token')
                .then((response)=>{
                    const headers =  {
                        'api_key' : `76f8a1fab09bc13f2e48be45689dd074`,
                        'Authorization' : `Bearer ${response}`
                    }
                    
                    return (
                        <Image
                            source={{
                                uri: imageUrl,
                                headers: headers
                            }}
                            style={styles.profileAvatar}
                            />
                    )
                })
        } catch (error) {
            throw error
        }
    }

    const setImageHeaders1 = async () => {
        try {
            const token = await AsyncStorage.getItem('@token')

            const headers = {
                'api_key' : `76f8a1fab09bc13f2e48be45689dd074`,
                'Authorization' : `Bearer ${token}`
            }

            setImageHeadersState(headers)
        } catch (error) {
            throw error
        }
    }

    const setState = (inputObject) => {
        console.log(inputObject)

        setFullName(inputObject.name)
        setUserName(inputObject.username)
        setEmailAddress(inputObject.email)
        setJobTitle(inputObject.jabatan)
        setNik(inputObject.nik)
        setPhoneNumber(inputObject.phone)
        setHomeBase(inputObject.homebase)
        setDateOfBirth(moment(inputObject.dob).format('DD MMMM YYYY'))
        setNationalIdentifier(inputObject.ktp_id)
        setAddress(inputObject.address)
        setFamilyIdentifier(inputObject.no_kk)

        if(inputObject.avatar){
            setAvatarIcon(inputObject.avatar)
        }

        const familyArray = inputObject.relation

        for (let i=0;i<familyArray.length;i++){
            switch ((familyArray[i].relation_status)) {
                case 'ayah':
                    setFatherName(familyArray[i].name)
                    setFatherAddress(familyArray[i].address)
                    break;
                case 'ibu':
                    setMotherName(familyArray[i].name)
                    setMotherAddress(familyArray[i].address)
                case 'spouse':
                    setSpouseName(familyArray[i].name)
                    setSpouseBirth(moment(familyArray[i].dob).format('DD MMMM YYYY'))
                case 'anak ke 1':
                    setFirstChildName(familyArray[i].name)
                    setFirstChildBirth(moment(familyArray[i].dob).format('DD MMMM YYYY'))
                case 'anak ke 2':
                    setSecondChildName(familyArray[i].name)
                    setSecondChildBirth(moment(familyArray[i].dob).format('DD MMMM YYYY'))
                default:
                    break;
            }
        }
    }

    const onFinishPickAvatar = async(imageBase64) => {
        try {
            const token = await AsyncStorage.getItem('@token')            

            await uploadAvatar(token, imageBase64)
                .then((response)=> {
                    if(response.status === 200) {
                        alert('Upload Successful')
                    }
                    
                    console.log(response)
                })

            await props.dispatchUserProfile(token)

        } catch (error) {
            throw error
        }
    }

    const logOutPressed = async() => {
        try {
            await AsyncStorage.clear()
        } catch (error) {
            throw error
        }
        props.navigation.replace('Login')
    }

    return (
        <View 
            style={styles.container}>
            {/* Header untuk Profile Picture, Nama Lengkap, Title */}
            <View style={styles.headerContainer}>
                <TouchableOpacity
                    onPress={()=>{
                        chooseAvatarImage()
                    }}>
                    <Image
                        source={{
                            uri: avatarIcon,
                            /*headers: {
                                'api_key' : `76f8a1fab09bc13f2e48be45689dd074`,
                                'Authorization' : `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjkzMTk2LCJpYXQiOjE2MTQzMzYxMTEsImV4cCI6MTYxNDQyMjUxMX0.e8nG82N_eklC21H-AIDe8IpK1X1cnIyK_DGhlvLuKmQ`,
                            },*/
                            headers: imageHeadersState
                        }}
                        //source={require('../../assets/images/avatar-user.png')}
                        style={styles.profileAvatar}/>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => logOutPressed()}>
                    <Icon
                        name={'exit'}                        
                        style={{ marginVertical: 10, }}
                        size={30}
                        color={colors.merah} />
                </TouchableOpacity>
                
                <Text
                    style={styles.profileName}>
                        {fullName}
                </Text>

                <Text
                    style={styles.profileSubTitle}>
                        {jobTitle}
                </Text>

                <Text
                    style={styles.profileSubTitle}>
                        {emailAddress}
                </Text>
            </View>
            
            {/* General Information */}
            <ScrollView style={styles.bodyContainer}>
                {/*----------General Information----------*/}
                <View style={styles.categoryTitleContainer}>
                    <Text
                        style={styles.categoryTextTitle}>
                            General Information
                    </Text>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Username</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{userName}</Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>NIK</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{nik}</Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Phone Number</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{phoneNumber}</Text>
                        </View>
                </View>

                {/*----------Privacy & Security Information----------*/}
                <View style={styles.categoryTitleContainer}>
                    <Text
                        style={styles.categoryTextTitle}>
                            Security & Privacy
                    </Text>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Password</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}></Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <View style={{flex: 1,}}/>
                        <Text style={{flex: 1, fontSize: 12,}}>Expired in {passwordExpiryCount} days</Text>
                        <TouchableOpacity 
                            style={styles.changeButton}
                            onPress={()=> setIsPasswordCardPressed(true)}>
                            <Text style={{color: colors.putih}}>Change</Text>
                        </TouchableOpacity> 
                </View>

                {/*----------Personal Information----------*/}
                <View style={styles.categoryTitleContainer}>
                    <Text
                        style={styles.categoryTextTitle}>
                            Personal Information
                    </Text>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Homebase</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{homeBase}</Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Date of Birth</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{dateOfBirth}</Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>National Identifier</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{nationalIdentifier}</Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Address</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{address}</Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <View style={{flex: 1,}}/>
                        <TouchableOpacity 
                            style={styles.changeButton}
                            onPress={()=> setIsAddressCardPressed(true)}>
                            <Text style={{color: colors.putih}}>Edit</Text>
                        </TouchableOpacity> 
                </View>

                {/*----------Contacts Information----------*/}
                <View style={styles.categoryTitleContainer}>
                    <Text
                        style={styles.categoryTextTitle}>
                            Contacts Information
                    </Text>
                </View>

                {/*----- Family Identifier / No KK -----*/}
                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Family Identifier</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{familyIdentifier}</Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <View style={{flex: 1,}}/>
                        <TouchableOpacity 
                            style={styles.changeButton}
                            onPress={()=> setIsFamilyCardPressed(true)}>
                            <Text style={{color: colors.putih}}>Edit</Text>
                        </TouchableOpacity> 
                </View>

                {/*----- Parents' Identity -----*/}
                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Father's name</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{fatherName}</Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Mother's Name</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{motherName}</Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Parents' Address</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{fatherAddress}</Text>
                        </View>
                </View>

                {/*----- Spouse's Identity -----*/}
                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Spouse's Name</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{spouseName}</Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Date of Birth</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{spouseBirth}</Text>
                        </View>
                </View>

                {/*----- Children's Identity -----*/}
                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>First Child</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{firstChildName}</Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Date of Birth</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{firsthChildBirth}</Text>
                        </View>
                </View>


                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Second Child</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{secondChildName}</Text>
                        </View>
                </View>

                <View
                    style={styles.formContainer}>
                        <Text style={styles.formTitle}>Date of Birth</Text>
                        <View style={styles.formBox}>
                            <Text style={styles.formText}>{secondChildBirth}</Text>
                        </View>
                </View>

                
            </ScrollView>

        {isPasswordCardPressed ? 
            <PasswordCard
                backPress={ ()=> {
                    setIsPasswordCardPressed(false)
                }}/> : null
        }

        {isAddressCardPressed ?
            <ChangeAddressCard
                nomorKK = {familyIdentifier}
                backPress={ async ()=> {
                    setIsAddressCardPressed(false)
                    const token = await AsyncStorage.getItem('@token')
                    props.dispatchUserProfile(token)
                }}/> : null
        }

        {isFamilyCardPressed ? 
            <ChangeFamilyIDCard
                address={address}
                backPress={ async ()=>{
                    setIsFamilyCardPressed(false)
                    const token = await AsyncStorage.getItem('@token')
                    props.dispatchUserProfile(token)
                }} /> : null
        
        }
        
                
        </View>
    )
}

function mapStateToProps(state) {
    return {
        userStore: state.userStore,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchUserProfile: (token) => dispatch(fetchUserProfile(token)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage)

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    headerContainer:{
        paddingTop: 80,
        alignItems: 'center',
    },
    profileAvatar:{
        width: 80,
        height: 80,
        borderRadius: 40,
    },
    profileName:{
        fontWeight: 'bold',
        fontSize: 20,
        marginTop: 20,
        marginHorizontal: 20,
    },
    profileSubTitle:{
        marginTop: 10,
    },

    bodyContainer:{
        //backgroundColor: colors.abuabutua,
        marginTop: 10,
    },
    categoryTitleContainer:{
        padding: 10,
        marginHorizontal: 20,
        marginTop: 20,
        marginBottom: 10,
        borderRadius: 100,
        backgroundColor: colors.merah,
    },
    categoryTextTitle:{
        color: colors.putih,
        textAlign: 'center',
    },
    formContainer:{
        flexDirection: 'row',
        marginHorizontal: 20,
        marginTop: 10,
        alignItems: 'center',
    },
    formTitle:{
        flex: 1,
    },
    formBox:{
        padding: 7,
        flex: 2,
        borderRadius: 15,
        backgroundColor: colors.abuabumuda,
    },
    formText:{
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: 'justify',
    },
    changeButton:{
        padding: 5,
        width: 100,
        borderRadius: 25,
        alignItems: 'center',
        backgroundColor: colors.abuabutua,
    },
})
