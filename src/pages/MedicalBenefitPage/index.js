import React, { useState, useCallback, useEffect } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Dimensions, FlatList } from 'react-native';
import HistoryMedicalCard from './../../components/HistoryMedicalCard';
import ClaimCard from './../../components/ClaimCard';
import DetailPlafonCard from './../../components/DetailPlafonCard';
import queryHistoryMedical from '../../Api/historyMedicalAPI';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import queryMedicalBalance from '../../Api/medicalBalanceAPI';
import { connect } from 'react-redux';
import fetchUserProfile from './../../stores/actions/userAction';


const { width, height } = Dimensions.get('window');
const MedicalBenefitPage = (props) => {
    const avatarIcon = props.userStore.payload.avatar ? props.userStore.payload.avatar : Image.resolveAssetSource(require('../../assets/images/avatar-user.png')).uri;
    const fullName = props.userStore.payload.name;
    const [isDetail, setIsDetail] = useState(false);
    const [isClaim, setIsClaim] = useState(false);
    const [birthDate, setBirthDate] = useState('');
    const [noPolis, setNoPolis] = useState('12345');
    const [balance, setBalance] = useState('10000000');
    const [refreshing, setRefreshing] = useState(false);
    const [historyData, setHistoryData] = useState([]);
    const [detailBalance, setDetailBalance] = useState([]);
    const [imageHeadersState, setImageHeadersState] = useState({});


    const currencyFormat = amount => {
        return Number(amount)
            .toFixed(2)
            .replace(/\d(?=(\d{3})+\.)/g, '$&,');
    };

    useEffect(() => {
        fetchDataMedical();
        fetchDataBalance();
        setImageHeaders();
    }, []);

    const onRefresh = useCallback(async () => {
        const token = await AsyncStorage.getItem('@token');
        setRefreshing(true);
        fetchDataMedical(token);
        fetchDataBalance(token);
        setRefreshing(false);
    }, []);

    const fetchDataMedical = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            await queryHistoryMedical(token)
                .then((response) => {
                    // console.warn(response.data)
                    if (response.data == null) {
                        console.log('fetch data medical failed');
                    } else {
                        !response.data ? console.warn(response.code)
                            : [setHistoryData(response.data.data.History),
                            setBirthDate(response.data.data.profile[0].dob),
                            setNoPolis(response.data.data.profile[0].insurance_num),
                            setBalance(response.data.data.Sisa_Plafon)];
                    }
                });
        } catch (error) {
            console.log('fetch data medical gagal');
        }
    };

    const fetchDataBalance = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            await queryMedicalBalance(token)
                .then((response) => {
                    // console.warn(response.data)
                    if (response.data == null) {
                        console.log('fetch data balance failed');
                    } else {
                        !response.data ? console.warn(response.code) : setDetailBalance(response.data.data);
                    }
                });
        } catch (error) {
            console.log('fetch data balance gagal');
        }
    };

    const setImageHeaders = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');

            const headers = {
                'api_key': `76f8a1fab09bc13f2e48be45689dd074`,
                'Authorization': `Bearer ${token}`
            };

            setImageHeadersState(headers);
        } catch (error) {
            throw error;
        }
    };

    return (
        <View>
            <View style={{ height: 0.25 * height, alignItems: 'center', justifyContent: 'flex-end' }}>
                <Image
                    source={{
                        uri: avatarIcon,
                        headers: imageHeadersState
                    }}
                    width={1}
                    height={1}
                    style={styles.avatar}
                />
                <Text style={{ textAlign: 'center', marginTop: 10, marginBottom: 5, fontSize: 20, fontWeight: 'bold' }}>{fullName}</Text>
            </View>
            <View style={{ height: 220,alignItems: 'center', marginBottom: 10 }}>
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                    <Text style={{ textAlign: 'center', fontSize: 15 }}>Birthdate : </Text>
                    <Text style={{ textAlign: 'center', fontSize: 15 }}>{moment(birthDate).format('DD MMMM YYYY')}</Text>
                </View>
                <Text style={{ textAlign: 'center', fontSize: 24, fontWeight: 'bold', marginTop: 10 }}>No. Polis : {noPolis}</Text>
                <View style={{ height: 80, width: 0.9 * width, borderRadius: 25, justifyContent: 'center', backgroundColor: '#A82929', marginTop: 10 }}>
                    <Text style={{ color: '#FFFFFF', textAlign: 'center', fontSize: 20 }}>Plafond Balance</Text>
                    <Text style={{ color: '#FFFFFF', textAlign: 'center', fontSize: 24, fontWeight: 'bold' }}>Rp. {currencyFormat(balance)}</Text>
                </View>
                <View style={{ width: 0.9 * width, flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() =>
                            // console.log('tekan');
                            setIsDetail(true)}>
                        <Text style={{ textAlign: 'center', color: '#FFFFFF', fontSize: 15 }}>Detail Plafon</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() =>
                            // console.log('tekan');
                            setIsClaim(true)}>
                        <Text style={{ textAlign: 'center', color: '#FFFFFF', fontSize: 15 }}>Claim</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ height: height*0.75, backgroundColor: '#A82929', borderTopLeftRadius: 50, borderTopRightRadius: 50 }}>
                <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 20, color: '#FFFFFF' }}>History Claim</Text>
                </View>
                <View style={{ flex: 0.9 }}>
                    <FlatList
                        data={historyData}
                        renderItem={({ item, index }) => {
                            return <HistoryMedicalCard
                                index={index}
                                data={item}
                                dateClaim={item.date}
                                typeClaim={item.benefit.type_name}
                                amountClaim={item.usage}
                            />;
                        }}
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                        // keyExtractor={(item) => item.type.toString() + item.date + item.usage}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </View>


            {isDetail ?
                <DetailPlafonCard
                    balance={detailBalance}
                    backPress={async () => {
                        try {
                            setIsDetail(false);
                            fetchDataMedical();
                            fetchDataBalance();
                        } catch (error) {
                            throw error;
                        }
                    }}
                />
                : null}
            {isClaim ?
                <ClaimCard
                    backPress={async () => {
                        try {
                            setIsClaim(false);
                            fetchDataMedical();
                            fetchDataBalance();
                        } catch (error) {
                            throw error;
                        }
                    }}
                />
                : null}
        </View>
    );
};

function mapStateToProps(state) {
    return {
        userStore: state.userStore
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchUserProfile: (token) => dispatch(fetchUserProfile(token))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MedicalBenefitPage);

const styles = StyleSheet.create({
    avatar: {
        height: 100,
        width: 100,
        borderRadius: 50
    },
    button: {

        justifyContent: 'center',
        backgroundColor: '#000000',
        borderRadius: 25,
        height: 50,
        width: 0.43 * width
    }
});
