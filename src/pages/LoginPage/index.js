import React,{ useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { colors } from '../../utils/colors';
import Input from '../../components/atoms/Input';
import doLogin from '../../Api/loginApi';
import AsyncStorage from '@react-native-async-storage/async-storage';

const LoginPage = (props) => {
    // const [valueEmail, setEmail] = useState('kristanto_r_winner@telkomsel.co.id');
    // const [valuePassword, setPassword] = useState('t4mpan123');
    const [valueEmail, setEmail] = useState('');
    const [valuePassword, setPassword] = useState('');
    const [notShownPassword, setNotShownPassword] = useState(true);


    const onPressedLogin = async(email,password) => {
        try {
            await doLogin(email,password)
                .then((response) => {
                    if (response == null){
                        alert(response.data.message);
                    } else {
                        AsyncStorage.setItem('@token',response.data.data.token);
                        AsyncStorage.setItem('@email',valueEmail);
                        props.navigation.replace('Home');
                    }
                });
        } catch (error) {
            alert(error.response.data.message);

        }
    };



    const forgetPassword = () => {
        props.navigation.navigate('ForgetPassword');
    };

    return (
        <ScrollView
            style={styles.container}>
                <View style={styles.logoContainer} >
                    <Image
                        source={require('../../assets/images/employee-icon.png')}
                        />
                    <Image
                        source={require('../../assets/images/app-title.png')} />
                </View>

                <View style={styles.formContainer}>
                    <Input
                        type="email"
                        placeholder="Username / E-mail Address"
                        value={valueEmail}
                        onChangeText={(value)=> {setEmail(value);}} />
                </View>

                <View style={styles.formContainer}>
                    <Input
                        type="password"
                        notShowPassword={notShownPassword}
                        onPress={()=> setNotShownPassword(!notShownPassword)}
                        value={valuePassword}
                        onChangeText={(value)=>{setPassword(value);}}
                        placeholder="Password" />
                </View>


                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        style={{ flex: 1 }}
                        onPress={forgetPassword}>
                        <Text style={styles.forgetText}>Forget Password?</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{ flex: 1, flexDirection: 'row-reverse' }}
                        onPress={()=>{
                            onPressedLogin(valueEmail, valuePassword);
                        }}>
                        <Image
                            source={require('../../assets/images/login-button.png')} />
                    </TouchableOpacity>
                </View>

        </ScrollView>
    );
};

export default LoginPage;


const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.merah,
        flex: 1
    },
    logoContainer: {
        marginLeft: 0,
        marginRight: 0,
        marginTop: 50,
        alignItems: 'center'
    },
    formContainer: {
        marginTop: 20,
        marginHorizontal: 20,
        padding: 5,
        borderRadius: 30,
        backgroundColor: colors.putih
    },
    inputForm: {
        marginTop: 20,
        padding: 15,
        backgroundColor: colors.putih,
        borderRadius: 30
    },
    buttonContainer: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 20
    },
    forgetText: {
        color: colors.putih,
        fontWeight: 'bold',
        fontSize: 14,
        alignContent: 'flex-end'
    }
});
