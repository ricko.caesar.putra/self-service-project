import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, Platform } from 'react-native';
import { colors } from '../../utils/colors';
import checkPasswordExpiry from '../../Api/checkPasswordExpiryApi';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { version } from "../../../package.json";
import checkVersion from '../../Api/checkVersionApi';
import VersionUpdateCard from '../../components/VersionUpdateCard';

const SplashScreen = (props) => {
    const [isUpdate,setIsUpdate] = useState(false);
    const [dataVersion,setDataVersion] = useState({});

    useEffect(() => {
        checkExpired();
    }, []);

    const checkExpired = async () => {
        try {
            if (Platform.OS === 'android') {
                const check_Version = await checkVersion(version, Platform.OS);
                if (check_Version.data.data[0].version !== version) {
                    setDataVersion(check_Version.data.data[0]);
                    return setIsUpdate(true);
                    // return alert(`${check_Version.data.message}, visit = ${check_Version.data.data[0].download_url}`);
                }
            }

            const token = await AsyncStorage.getItem('@token');

            if (token == null) {
                props.navigation.replace('Login');
            }

            await checkPasswordExpiry(token)
                .then((response) => {
                    if (response.status === 401) {
                        props.navigation.replace('Login');
                    } else if (response.status === 203) {
                        props.navigation.replace('Home');
                    } else (
                        props.navigation.replace('Login')
                    );
                })
                .catch(() => {
                    props.navigation.replace('Login');
                });
        } catch (error) {
            throw error;
        }
    };

    return (
        <View style={styles.container}>
            <Image
                source={require('../../assets/images/employee-icon.png')} />
            <Image
                source={require('../../assets/images/app-title.png')} />

            {isUpdate ?
            <VersionUpdateCard
            curVer={version}
            version={dataVersion}
            backPress={()=>null}
            />
            : null}
        </View>
    );
};

export default SplashScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.merah,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
