import _ from 'lodash';
import React, { Component } from 'react';
import { Platform, Alert, StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { ExpandableCalendar, AgendaList, CalendarProvider, WeekCalendar } from 'react-native-calendars';
import { Paragraph, Modal, Portal, Dialog, FAB, Provider, TextInput, Button } from 'react-native-paper';
import { colors } from "../../utils";
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import getReminder from '../../Api/reminderApi';
import saveReminder from '../../Api/saveReminderApi';
import deleteReminder from '../../Api/deleteReminderApi';
import editReminder from '../../Api/editReminderApi';
import DateTimePickerModal from "react-native-modal-datetime-picker";

const testIDs = require('./testIDs');
console.disableYellowBox = true;
const today = new Date().toISOString().split('T')[0];
const fastDate = getPastDate(3);
const futureDates = getFutureDates(9);
const dates = [fastDate, today].concat(futureDates);
const themeColor = '#00AAAF';
const lightThemeColor = '#EBF9F9';

function getFutureDates(days) {
    const array = [];
    for (let index = 1; index <= days; index++) {
        const date = new Date(Date.now() + 864e5 * index); // 864e5 == 86400000 == 24*60*60*1000
        const dateString = date.toISOString().split('T')[0];
        array.push(dateString);
    }
    return array;
}

function getPastDate(days) {
    return new Date(Date.now() - 864e5 * days).toISOString().split('T')[0];
}



export default class ReminderPage extends Component {

    // buat constructor untuk load data pertama ketika component terbentuk

    constructor(props) {

        super(props);

        // set state untuk nampung notes, state awal dikasi data dummy

        this.state = {
            ITEMS: [{
                title: dates[0],
                data: [{
                    id: 0,
                    hour: '',
                    duration: '',
                    timeStart: '',
                    timeStop: '',
                    dataStart: '',
                    dataStop: '',
                    title: 'Loading Reminder...'
                }]
            }],
            markedDates: {},
            visible: false,
            setVisible: false,
            open: false,
            note: '',
            pickedDate: today,
            containerStyle: { backgroundColor: 'white', padding: 20 },
            openModalReminder: false,
            modalReminder: {
                id: 0,
                title: '',
                dateStart: '',
                dateStop: '',
                duration: '',
                remainingHour: ''
            },
            pickerDate: new Date().toISOString(),
            pickerTime: new Date().toISOString(),
            pickerDate2: new Date().toISOString(),
            pickerTime2: new Date().toISOString(),
            mode: 'date',
            modeTime: 'time',
            showPicker: true,
            descriptionNew: '',
            descriptionEdit: '',
            tokie: '',
            isDatePickerVisible: false,
            isTimePickerVisible: false,
            isDatePickerVisible2: false,
            isTimePickerVisible2: false
        };

        //get async storage token yang tersimpan

        AsyncStorage.getItem('@token').then(async (token) => {
            this.setState({ tokie: token });
            // call api getNotes dari calendarApi.js ke server
            await getReminder(token).then((response) => {
                let data = response.data.data;
                let y = [];
                let markers = {};

                for (let i = 0; i < data.length; i++) {

                    let dateStart = data[i].time_start.split('T');
                    let dateEnd = data[i].time_stop.split('T');

                    let duration = moment().diff(moment(data[i].time_stop), 'hours', true);
                    let rangeDate = moment(dateEnd[0], 'YYYY-MM-DD').format('DD MMM YYYY');
                    let startDate = moment(dateStart[0], 'YYYY-MM-DD').format('DD MMM YYYY');

                    y.push({
                        title: dateStart[0],
                        data: [{
                            id: data[i].id,
                            timeStart: startDate,
                            dataStart: data[i].time_start,
                            dataStop: data[i].time_stop,
                            timeStop: rangeDate,
                            hour: 'Until ' + rangeDate,
                            duration: Math.abs(Math.round(duration)) + ' hours to go',
                            title: data[i].description
                        }]
                    });

                    let diff = moment(data[i].time_start).diff(moment(data[i].time_stop), 'days');

                    markers[dateStart[0]] = {
                        startingDay: true, color: '#50cebb', textColor: 'white'
                    };

                    for (let j = 0; j < Math.abs(diff); j++) {
                        let parsed = moment(data[i].time_start).add(j + 1, 'd').format('YYYY-MM-DD');

                        if (dateStart[0] == parsed) {
                            markers[parsed] = {
                                startingDay: true, color: '#50cebb', textColor: 'white'
                            };

                        } else if (dateEnd[0] == parsed) {
                            markers[parsed] = {
                                endingDay: true, color: '#50cebb', textColor: 'white'
                            };

                        } else {
                            markers[parsed] = {
                                color: '#50cebb', textColor: 'white'
                            };
                        }

                    }

                }

                if (y.length > 0)
                    this.setState({ ITEMS: y });
                this.setState({ markedDates: markers });


            });


        });

        //other command

    }


    showDialog = () => {
        this.setState({ visible: true });
        this.hideButton();
    };
    showButton = () => this.setState({ open: !this.state.open });
    hideButton = () => this.setState({ open: false });
    hideDialog = () => this.setState({ visible: false });

    hideReminderDetails = () => this.setState({ openModalReminder: false });

    async deleteOne(id) {
        let token = await AsyncStorage.getItem('@token');
        this.setState({
            openModal: false
        })
        await deleteReminder(token, id).then((response) => {
            this.getAllReminder(token);
        });
    }

    getAllReminder = async (token) => {

        // call api getNotes dari calendarApi.js ke server
        await getReminder(token).then((response) => {
            let data = response.data.data;
            let y = [];
            let markers = {};

            for (let i = 0; i < data.length; i++) {

                let dateStart = data[i].time_start.split('T');
                let dateEnd = data[i].time_stop.split('T');

                let duration = moment().diff(moment(data[i].time_stop), 'hours', true);
                let rangeDate = moment(dateEnd[0], 'YYYY-MM-DD').format('DD MMM YYYY');
                let startDate = moment(dateStart[0], 'YYYY-MM-DD').format('DD MMM YYYY');

                y.push({
                    title: dateStart[0],
                    data: [{
                        id: data[i].id,
                        timeStart: startDate,
                        timeStop: rangeDate,
                        dataStart: data[i].time_start,
                        dataStop: data[i].time_stop,
                        hour: 'Until ' + rangeDate,
                        duration: Math.abs(Math.round(duration)) + ' hours to go',
                        title: data[i].description
                    }]
                });

                let diff = moment(data[i].time_start).diff(moment(data[i].time_stop), 'days');

                markers[dateStart[0]] = {
                    startingDay: true, color: '#50cebb', textColor: 'white'
                };

                for (let j = 0; j < Math.abs(diff); j++) {
                    let parsed = moment(data[i].time_start).add(j + 1, 'd').format('YYYY-MM-DD');

                    if (dateStart[0] == parsed) {
                        markers[parsed] = {
                            startingDay: true, color: '#50cebb', textColor: 'white'
                        };

                    } else if (dateEnd[0] == parsed) {
                        markers[parsed] = {
                            endingDay: true, color: '#50cebb', textColor: 'white'
                        };

                    } else {
                        markers[parsed] = {
                            color: '#50cebb', textColor: 'white'
                        };
                    }

                }

            }

            if (y.length > 0)
                this.setState({ ITEMS: y });

            this.setState({ markedDates: markers });
            this.hideDialog();
            this.hideReminderDetails();
        });
    }

    saveRem = async () => {

        let dateStart = moment(this.state.pickerDate).format('YYYY-MM-DD').toString();
        let dateStop = moment(this.state.pickerDate2).format('YYYY-MM-DD').toString();
        let timeStart = moment(this.state.pickerTime).format('HH:mm:ss').toString();
        let timeStop = moment(this.state.pickerTime2).format('HH:mm:ss').toString();

        let token = await AsyncStorage.getItem('@token');
        await saveReminder(token, dateStart, dateStop, timeStart, timeStop, this.state.descriptionNew).then((response) => {
            // alert(response.data.message);
            console.log(token)
            this.getAllReminder(token);
            alert(response.data.message)
        }, (err) => {
            this.getAllReminder(token);
            alert(err.response.data.message);
        });
    }

    editRem = async () => {
        let dateStart = moment(this.state.pickerDate).format('YYYY-MM-DD').toString();
        let dateStop = moment(this.state.pickerDate2).format('YYYY-MM-DD').toString();
        let timeStart = moment(this.state.pickerTime).format('HH:mm:ss').toString();
        let timeStop = moment(this.state.pickerTime2).format('HH:mm:ss').toString();

        let id = this.state.modalReminder.id;

        let token = await AsyncStorage.getItem('@token');
        await editReminder(token, id, dateStart, dateStop, timeStart, timeStop, this.state.descriptionEdit).then((response) => {
            this.getAllReminder(token);
            alert(response.data.message)
        }, (err) => {
            this.getAllReminder(token);
            alert(err.response.data.message);
        });
    }

    backToNotes() {
        this.props.navigation.navigate('CalendarPage');
    }

    onDateChanged = async (date, updateSource) => {
        this.setState({ pickedDate: date });
    };

    onMonthChange = (/* month, updateSource */) => {
        // console.warn('ExpandableCalendarScreen onMonthChange: ', month, updateSource);
    };

    async onNoteSave() {
        // call api
    }


    showDatePicker = () => {
        this.setState({ isDatePickerVisible: true });
    };

    showTimePicker = () => {
        this.setState({ isTimePickerVisible: true });
    };

    showDatePicker2 = () => {
        this.setState({ isDatePickerVisible2: true });
    };

    showTimePicker2 = () => {
        this.setState({ isTimePickerVisible2: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDatePickerVisible: false });
        this.setState({ isTimePickerVisible: false });
        this.setState({ isDatePickerVisible2: false });
        this.setState({ isTimePickerVisible2: false });
    };

    handleDatePicked = date => {
        console.log("A date has been picked: ", date.toISOString());
        this.setState({ pickerDate: date.toISOString() });
        this.hideDateTimePicker();
    };

    handleTimePicked = time => {
        console.log("A time has been picked: ", time.toISOString());
        this.setState({ pickerTime: time.toISOString() });
        this.hideDateTimePicker();
    };

    handleDatePicked2 = date => {
        console.log("A date has been picked: ", date.toISOString());
        this.setState({ pickerDate2: date.toISOString() });
        this.hideDateTimePicker();
    };

    handleTimePicked2 = time => {
        console.log("A time has been picked: ", time.toISOString());
        this.setState({ pickerTime2: time.toISOString() });
        this.hideDateTimePicker();
    };


    buttonPressed() {
        Alert.alert('show more');
    }

    itemPressed(item) {

        let duration = moment(item.timeStart).diff(moment(item.timeStop), 'hours');
        let remaining = moment().diff(moment(item.timeStop), 'hours');

        this.setState({
            openModalReminder: true,
            modalReminder: {
                id: item.id,
                title: item.title,
                dateStart: item.timeStart,
                dateStop: item.timeStop,
                duration: duration,
                remainingHour: remaining
            }
        });

        this.setState({ pickerDate: item.dataStart });
        this.setState({ pickerDate2: item.dataStop });
        this.setState({ pickerTime: item.dataStart });
        this.setState({ pickerTime2: item.dataStop });

        this.setState({ descriptionEdit: item.title });
    }

    renderEmptyItem() {
        return (
            <View style={styles.emptyItem}>
                <Text style={styles.emptyItemText}>No Events Planned</Text>
            </View>
        );
    }

    renderItem = ({ item }) => {
        if (_.isEmpty(item)) {
            return this.renderEmptyItem();
        }

        return (
            <TouchableOpacity onPress={() => this.itemPressed(item)} style={styles.item} testID={testIDs.agenda.ITEM}>
                <View>
                    <Text style={styles.itemHourText}>{item.hour}</Text>
                    <Text style={styles.itemDurationText}>{item.duration}</Text>
                </View>
                <Text style={styles.itemTitleText}>{item.title}</Text>
                <View style={styles.itemButtonContainer}>
                    <Button color={'grey'} title={'Info'} onPress={() => this.deleteOne(item.id)}>
                        Delete
                    </Button>
                </View>
            </TouchableOpacity>
        );
    };

    getMarkedDates = () => {
        const marked = {};
        this.state.ITEMS.forEach(item => {
            // NOTE: only mark dates with data
            if (item.data && item.data.length > 0 && !_.isEmpty(item.data[0])) {
                marked[item.title] = { marked: true };
            } else {
                marked[item.title] = { disabled: true };
            }
        });
        return marked;
    };

    render() {
        return (
            <Provider>
                <CalendarProvider
                    date={this.state.ITEMS[0].title}

                    onDateChanged={this.onDateChanged}
                    onMonthChange={this.onMonthChange}
                    showTodayButton
                    disabledOpacity={0.6}

                >
                    <Portal>
                        <Modal visible={this.state.openModalReminder} onDismiss={this.hideReminderDetails} contentContainerStyle={this.state.containerStyle}>
                            <View>
                                <Text>Reminder</Text>
                            </View>

                            <View style={{ marginTop: 10 }}>
                                <Text>Date Start</Text>
                                <TouchableOpacity onPress={this.showDatePicker} >
                                    <Text>{moment(this.state.pickerDate).format('YYYY-MM-DD').toString()}</Text>
                                </TouchableOpacity>
                                <DateTimePickerModal
                                    isVisible={this.state.isDatePickerVisible}
                                    mode="date"
                                    onConfirm={this.handleDatePicked}
                                    onCancel={this.hideDateTimePicker}
                                />
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>Time Start</Text>
                                <TouchableOpacity onPress={this.showTimePicker} >
                                    <Text>{moment(this.state.pickerTime).format('HH:mm').toString()}</Text>
                                </TouchableOpacity>
                                <DateTimePickerModal
                                    isVisible={this.state.isTimePickerVisible}
                                    mode="time"
                                    onConfirm={this.handleTimePicked}
                                    onCancel={this.hideDateTimePicker}
                                />
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>Date Stop</Text>
                                <TouchableOpacity onPress={this.showDatePicker2} >
                                    <Text>{moment(this.state.pickerDate2).format('YYYY-MM-DD').toString()}</Text>
                                </TouchableOpacity>
                                <DateTimePickerModal
                                    isVisible={this.state.isDatePickerVisible2}
                                    mode="date"
                                    onConfirm={this.handleDatePicked2}
                                    onCancel={this.hideDateTimePicker}
                                />
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>Time Stop</Text>
                                <TouchableOpacity onPress={this.showTimePicker2} >
                                    <Text>{moment(this.state.pickerTime2).format('HH:mm').toString()}</Text>
                                </TouchableOpacity>
                                <DateTimePickerModal
                                    isVisible={this.state.isTimePickerVisible2}
                                    mode="time"
                                    onConfirm={this.handleTimePicked2}
                                    onCancel={this.hideDateTimePicker}
                                />
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <TextInput
                                    label="Description"
                                    mode="outlined"
                                    value={this.state.descriptionEdit}
                                    onChangeText={text => this.setState({ descriptionEdit: text })} />
                            </View>
                            <Dialog.Actions>
                                <Button onPress={this.editRem}>Edit</Button>
                                <Button onPress={this.hideReminderDetails}>Close</Button>
                            </Dialog.Actions>


                        </Modal>
                    </Portal>


                    <Portal>
                        <Modal visible={this.state.visible} onDismiss={this.hideDialog} contentContainerStyle={this.state.containerStyle}>
                            <View>
                                <Text>New Reminder</Text>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>Date Start</Text>
                                <TouchableOpacity onPress={this.showDatePicker} >
                                    <Text>{moment(this.state.pickerDate).format('YYYY-MM-DD').toString()}</Text>
                                </TouchableOpacity>
                                <DateTimePickerModal
                                    isVisible={this.state.isDatePickerVisible}
                                    mode="date"
                                    onConfirm={this.handleDatePicked}
                                    onCancel={this.hideDateTimePicker}
                                />
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>Time Start</Text>
                                <TouchableOpacity onPress={this.showTimePicker} >
                                    <Text>{moment(this.state.pickerTime).format('HH:mm').toString()}</Text>
                                </TouchableOpacity>
                                <DateTimePickerModal
                                    isVisible={this.state.isTimePickerVisible}
                                    mode="time"
                                    onConfirm={this.handleTimePicked}
                                    onCancel={this.hideDateTimePicker}
                                />
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>Date Stop</Text>
                                <TouchableOpacity onPress={this.showDatePicker2} >
                                    <Text>{moment(this.state.pickerDate2).format('YYYY-MM-DD').toString()}</Text>
                                </TouchableOpacity>
                                <DateTimePickerModal
                                    isVisible={this.state.isDatePickerVisible2}
                                    mode="date"
                                    onConfirm={this.handleDatePicked2}
                                    onCancel={this.hideDateTimePicker}
                                />
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Text>Time Stop</Text>
                                <TouchableOpacity onPress={this.showTimePicker2} >
                                    <Text>{moment(this.state.pickerTime2).format('HH:mm').toString()}</Text>
                                </TouchableOpacity>
                                <DateTimePickerModal
                                    isVisible={this.state.isTimePickerVisible2}
                                    mode="time"
                                    onConfirm={this.handleTimePicked2}
                                    onCancel={this.hideDateTimePicker}
                                />
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <TextInput
                                    label="Description"
                                    mode="outlined"
                                    value={this.state.descriptionNew}
                                    onChangeText={text => this.setState({ descriptionNew: text })} />
                            </View>
                            <Dialog.Actions>
                                <Button onPress={this.saveRem}>Save</Button>
                                <Button onPress={this.hideDialog}>Close</Button>
                            </Dialog.Actions>
                        </Modal>
                    </Portal>

                    <Portal>
                        <FAB.Group
                            open={this.state.open}
                            icon={this.state.open ? 'bell' : 'bell'}
                            actions={[
                                { icon: 'plus', label: 'New Reminder', onPress: () => this.showDialog() },
                                { icon: 'arrow-left', label: 'Back to Agenda', onPress: () => this.backToNotes() },
                            ]}
                            onStateChange={this.showButton}
                            onPress={() => {
                                if (this.state.open) {
                                    this.setState({ open: false })
                                }
                            }}
                        />
                    </Portal>
                    <View style={styles.wrapperAtas}>
                        <Text style={styles.textAtas}>Reminder</Text>
                    </View>

                    {this.props.weekView ? (
                        <WeekCalendar
                            testID={testIDs.weekCalendar.CONTAINER}
                            firstDay={1}
                            markedDates={this.state.markedDates} />
                    ) : (
                            <ExpandableCalendar
                                testID={testIDs.expandableCalendar.CONTAINER}

                                disableAllTouchEventsForDisabledDays
                                firstDay={1}
                                markingType={'period'}
                                markedDates={this.state.markedDates}
                            />
                        )}
                    <AgendaList
                        sections={this.state.ITEMS}
                        extraData={this.state}
                        renderItem={this.renderItem}
                    />
                </CalendarProvider>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    calendar: {
        paddingLeft: 20,
        paddingRight: 20
    },
    section: {
        backgroundColor: lightThemeColor,
        color: 'grey',
        textTransform: 'capitalize'
    },
    item: {
        padding: 20,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        flexDirection: 'row'
    },
    itemHourText: {
        color: 'black'
    },
    itemDurationText: {
        color: 'grey',
        fontSize: 12,
        marginTop: 4,
        marginLeft: 4
    },
    itemTitleText: {
        color: 'black',
        marginLeft: 16,
        fontWeight: 'bold',
        fontSize: 16
    },
    itemButtonContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    emptyItem: {
        paddingLeft: 20,
        height: 52,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey'
    },
    emptyItemText: {
        color: 'lightgrey',
        fontSize: 14
    },
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20,
    },
    textAtas: {
        marginTop: 20,
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        color: '#FFFFFF'
    },
    buttonModal: {
        width: 15
    }
});