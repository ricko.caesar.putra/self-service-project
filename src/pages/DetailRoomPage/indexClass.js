import _ from 'lodash';
import React, { Component } from 'react';
import { Platform, Alert, StyleSheet, View, Text, TouchableOpacity, Button } from 'react-native';
import { ExpandableCalendar, AgendaList, CalendarProvider, WeekCalendar } from 'react-native-calendars';
import { colors } from "../../utils";

const testIDs = require('./testIDs');

const today = new Date().toISOString().split('T')[0];
const fastDate = getPastDate(1);
const futureDates = getFutureDates(9);
const dates = [fastDate, today].concat(futureDates);
const themeColor = '#00AAAF';
const lightThemeColor = '#EBF9F9';


function getFutureDates(days) {
    const array = [];
    for (let index = 1; index <= days; index++) {
        const date = new Date(Date.now() + 864e5 * index); // 864e5 == 86400000 == 24*60*60*1000
        const dateString = date.toISOString().split('T')[0];
        array.push(dateString);
    }
    return array;
}

function getPastDate(days) {
    return new Date(Date.now() - 864e5 * days).toISOString().split('T')[0];
}

const ITEMS = [
    { title: dates[0], data: [{ hour: '12am', duration: '1h', title: 'Ngoding' }] },
    {
        title: dates[1],
        data: [
            { hour: '4pm', duration: '1h', title: 'Ngoding ABC' },
            { hour: '5pm', duration: '1h', title: 'Ngoding lagi' }
        ]
    },
    {
        title: dates[2],
        data: [
            { hour: '1pm', duration: '1h', title: 'Ngoding lagi' },
            { hour: '2pm', duration: '1h', title: 'Ngoding' },
            { hour: '3pm', duration: '1h', title: 'Ngoding' }
        ]
    },
    { title: dates[3], data: [{ hour: '12am', duration: '1h', title: 'Ngoding' }] },
    { title: dates[4], data: [{}] },
    {
        title: dates[5],
        data: [
            { hour: '9pm', duration: '1h', title: 'Ngoding' },
            { hour: '10pm', duration: '1h', title: 'Ngoding' },
            { hour: '11pm', duration: '1h', title: 'Ngoding' },
            { hour: '12pm', duration: '1h', title: 'Ngoding Group' }
        ]
    },
    { title: dates[6], data: [{ hour: '12am', duration: '1h', title: 'Ngoding' }] },
    { title: dates[7], data: [{}] },
    {
        title: dates[8],
        data: [
            { hour: '9pm', duration: '1h', title: 'Ngoding' },
            { hour: '10pm', duration: '1h', title: 'Ngoding' },
            { hour: '11pm', duration: '1h', title: 'Ngoding' },
            { hour: '12pm', duration: '1h', title: 'Ngoding' }
        ]
    },
    {
        title: dates[9],
        data: [
            { hour: '1pm', duration: '1h', title: 'Ngoding' },
            { hour: '2pm', duration: '1h', title: 'Ngoding' },
            { hour: '3pm', duration: '1h', title: 'Ngoding' }
        ]
    },
    { title: dates[10], data: [{ hour: '12am', duration: '1h', title: 'Last Ngoding' }] }
];

export default class ExpandableCalendarScreen extends Component {

    onDateChanged = () => {
        // const cek=bookingTime.map((e)=>{
        //     return e.time
        // })
        // console.warn(cek)
        // console.warn('ExpandableCalendarScreen onDateChanged: ', date, updateSource);
        // fetch and set data for date + week ahead
    };

    onMonthChange = (/* month, updateSource */) => {
        // console.warn('ExpandableCalendarScreen onMonthChange: ', month, updateSource);
    };

    buttonPressed() {
        Alert.alert('show more');
    }

    itemPressed(id) {
        Alert.alert(id);
    }

    renderEmptyItem() {
        return (
            <View style={styles.emptyItem}>
                <Text style={styles.emptyItemText}>No Events Planned</Text>
            </View>
        );
    }

    renderItem = ({ item }) => {
        if (_.isEmpty(item)) {
            return this.renderEmptyItem();
        }

        return (
            <TouchableOpacity onPress={() => this.itemPressed(item.title)} style={styles.item} testID={testIDs.agenda.ITEM}>
                <View>
                    <Text style={styles.itemHourText}>{item.hour}</Text>
                    <Text style={styles.itemDurationText}>{item.duration}</Text>
                </View>
                <Text style={styles.itemTitleText}>{item.title}</Text>
                <View style={styles.itemButtonContainer}>
                    <Button color={'grey'} title={'Info'} onPress={this.buttonPressed} />
                </View>
            </TouchableOpacity>
        );
    };

    getMarkedDates = () => {
        const marked = {};
        ITEMS.forEach(item => {
            // NOTE: only mark dates with data
            if (item.data && item.data.length > 0 && !_.isEmpty(item.data[0])) {
                marked[item.title] = { marked: true };
            } else {
                marked[item.title] = { disabled: true };
            }
        });
        return marked;
    };

    render() {
        // const { navigation } = this.props;
        return (

            <CalendarProvider
                date={ITEMS[0].title}
                onDateChanged={this.onDateChanged}
                onMonthChange={this.onMonthChange}
                showTodayButton
                disabledOpacity={0.6}
            >
                <View style={styles.wrapperAtas}>
                    <Text style={styles.textAtas}>{this.props.route.params.name}</Text>
                </View>
                {this.props.weekView ? (
                    <WeekCalendar testID={testIDs.weekCalendar.CONTAINER} firstDay={1} markedDates={this.getMarkedDates()} />
                ) : (
                        <ExpandableCalendar
                            testID={testIDs.expandableCalendar.CONTAINER}
                            disableAllTouchEventsForDisabledDays
                            firstDay={1}
                            markedDates={this.getMarkedDates()}
                            leftArrowImageSource={url = 'https://freepngimg.com/download/previous_button/25730-4-previous-button-transparent-background.png'}
                            rightArrowImageSource={url = 'https://toppng.com/uploads/preview/ext-button-transparent-image-next-button-115629752231qqkzux7gn.png'}
                        />
                    )}
                <AgendaList
                    sections={ITEMS}
                    extraData={this.state}
                    renderItem={this.renderItem}
                />
            </CalendarProvider>

        );
    }
}

const styles = StyleSheet.create({
    calendar: {
        paddingLeft: 20,
        paddingRight: 20
    },
    section: {
        backgroundColor: lightThemeColor,
        color: 'grey',
        textTransform: 'capitalize'
    },
    item: {
        padding: 20,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        flexDirection: 'row'
    },
    itemHourText: {
        color: 'black'
    },
    itemDurationText: {
        color: 'grey',
        fontSize: 12,
        marginTop: 4,
        marginLeft: 4
    },
    itemTitleText: {
        color: 'black',
        marginLeft: 16,
        fontWeight: 'bold',
        fontSize: 16
    },
    itemButtonContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    emptyItem: {
        paddingLeft: 20,
        height: 52,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey'
    },
    emptyItemText: {
        color: 'lightgrey',
        fontSize: 14
    },
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20,
    },
    textAtas: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'

    }
});