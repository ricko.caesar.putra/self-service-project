import React, { useState, useRef } from 'react';
import { Dimensions, StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';
import Icon from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { colors } from '../../utils';

const { width, height } = Dimensions.get('screen');
const DisplayWeb = ({ navigation, route }) => {
    const { url, title } = route.params;

    const webViewRef = useRef();
    const [canGoBack, setCanGoBack] = useState(false);
    const [canGoForward, setCanGoForward] = useState(false);

    const handleBackPress = () => {
        webViewRef.current.goBack();
    };
    const handleForwardPress = () => {
        webViewRef.current.goForward();
    };

    return (
        <View style={{
            height: height
        }}>
            <View style={styles.wrapper}>
                <Icon style={{marginTop:35}}name="md-arrow-back" size={32} color={"#FFFFFF"} onPress={() => navigation.goBack()} />
                <Text
                    style={styles.judul}
                    numberOfLines={1}
                    ellipsizeMode="tail"
                >{title}</Text>
            </View>
            <WebView
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{ uri: url }}
                originWhitelist={['*']}
                ref={webViewRef}
                onNavigationStateChange={(state) => {
                    const back = state.canGoBack;
                    const forward = state.canGoForward;
                    setCanGoBack(back);
                    setCanGoForward(forward);
                }}
            />

            <View style={[styles.NavigasiBawah, !canGoBack && !canGoForward && styles.hide]}>
                {canGoBack &&
                    (<TouchableOpacity onPress={handleBackPress}>
                        <Text style={styles.buttonTitle}>
                            Back
                        </Text>
                    </TouchableOpacity>)}
                {canGoForward &&
                    (<TouchableOpacity onPress={handleForwardPress}>
                        <Text style={styles.buttonTitle}>
                            Forward
                        </Text>
                    </TouchableOpacity>)}

            </View>

        </View>
    );
};

export default DisplayWeb;

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingHorizontal: 40

    },
    judul: {
        width: '70%',
        height: 40,
        marginTop: 30,
        color:"#FFFFFF",
        fontSize: 20,
        fontWeight: 'bold',
        textAlignVertical: 'center',
        backgroundColor: colors.merah
    },
    NavigasiBawah: {
        height: 50,
        backgroundColor: colors.default,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    buttonTitle: {
        color: '#000',
        fontSize: 20
    },
    hide: { display: 'none' }
}
);
