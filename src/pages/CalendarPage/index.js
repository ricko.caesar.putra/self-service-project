import _ from 'lodash';
import React, { Component } from 'react';
import { Alert, StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { ExpandableCalendar, AgendaList, CalendarProvider, WeekCalendar } from 'react-native-calendars';
import { Modal, Portal, Dialog, FAB, Provider, TextInput, Button } from 'react-native-paper';
import { colors } from '../../utils';
import AsyncStorage from '@react-native-async-storage/async-storage';
import getNotes from '../../Api/calendarApi';
import saveNote from '../../Api/saveNoteApi';
import deleteNote from '../../Api/deleteCalendarApi';
import editNote from '../../Api/editCalendarApi';

const testIDs = require('./testIDs');
console.disableYellowBox = true;
const today = new Date().toISOString().split('T')[0];
const fastDate = getPastDate(3);
const futureDates = getFutureDates(9);
const dates = [fastDate, today].concat(futureDates);
const lightThemeColor = '#EBF9F9';

function getFutureDates(days) {
    const array = [];
    for (let index = 1; index <= days; index++) {
        const date = new Date(Date.now() + 864e5 * index); // 864e5 == 86400000 == 24*60*60*1000
        const dateString = date.toISOString().split('T')[0];
        array.push(dateString);
    }
    return array;
}

function getPastDate(days) {
    return new Date(Date.now() - 864e5 * days).toISOString().split('T')[0];
}



export default class CalendarPage extends Component {


    constructor(props) {

        super(props);

        // set state untuk nampung notes, state awal dikasi data dummy

        this.state = {
            ITEMS: [{
                title: dates[0],
                data: [{ date: '', id: 0, date: '', hour: '', duration: '', title: 'Loading Note...' }]
            }],
            visible: false,
            setVisible: false,
            open: false,
            openModal: false,
            note: '',
            pickedDate: today,
            modalData: {
                date: '',
                note: '',
                id: 0
            },
            containerStyle: { backgroundColor: 'white', padding: 20 }
        };

        //get async storage token yang tersimpan

        AsyncStorage.getItem('@token').then(async (token) => {

            // call api getNotes dari calendarApi.js ke server

            await getNotes(token).then((response) => {

                let { data } = response.data; //
                let x = [];

                for (let i = 0; i < data.length; i++) {
                    x.push({
                        title: data[i].date,
                        data: [{ date: data[i].date, id: data[i].id, hour: '', duration: '', title: data[i].description }]
                    });
                }

                if (x.length > 0) { this.setState({ ITEMS: x }); }

            }, (err) => {
                // error dari server tampilkan di sini
                console.log(err);
            });
        });

        //other command

    }

    getAllNotes = async (token) => {

        await getNotes(token).then((response) => {

            let { data } = response.data; //
            let x = [];


            for (let i = 0; i < data.length; i++) {
                x.push({
                    title: data[i].date,
                    data: [{ date: data[i].date, id: data[i].id, hour: '', duration: '', title: data[i].description }]
                });
            }

            if (x.length > 0) { this.setState({ ITEMS: x }); }

            this.hideDialog();

        }, (err) => {
            // error dari server tampilkan di sini
            console.log(err);
        });
    }



    showDialog = () => {
        this.setState({ visible: true });
        this.hideButton();
    };
    showButton = () => this.setState({ open: !this.state.open });
    hideButton = () => this.setState({ open: false });
    hideDialog = () => this.setState({ visible: false });
    hideModal = () => this.setState({ openModal: false });

    gotoReminder = () => {
        this.props.navigation.navigate('MyTaskPage');
    }

    deleteCalendar = async () => {
        let token = await AsyncStorage.getItem('@token');
        this.setState({
            openModal: false
        });
        await deleteNote(token, this.state.modalData.id).then(() => {
            this.getAllNotes(token);
        });
    }

    editCalendar = async () => {
        let token = await AsyncStorage.getItem('@token');
        this.setState({
            openModal: false
        });
        await editNote(token, this.state.modalData.id, this.state.modalData.date, this.state.modalData.note).then((response) => {
            this.getAllNotes(token);
            alert(response.data.message);
        }, (err) => {
            this.getAllNotes(token);
            alert(err.response.data.message);
        });
    }



    saveNote = async () => {
        let token = await AsyncStorage.getItem('@token');
        await saveNote(token, this.state.pickedDate, this.state.note).then((response) => {
            this.getAllNotes(token);
            alert(response.data.message);
        }, (err) => {
            this.getAllNotes(token);
            alert(err.response.data.message);
        });
    }

    onDateChanged = async (date) => {
        this.setState({ pickedDate: date });
    };

    onMonthChange = (/* month, updateSource */) => {
        // console.warn('ExpandableCalendarScreen onMonthChange: ', month, updateSource);
    };

    async onNoteSave() {
        // call api
    }


    buttonPressed() {
        Alert.alert('show more');
    }

    itemPressed(item) {
        this.setState({
            modalData: {
                date: item.date,
                note: item.title,
                id: item.id
            }
        });
        this.setState({
            openModal: true
        });
    }

    renderEmptyItem() {
        return (
            <View style={styles.emptyItem}>
                <Text style={styles.emptyItemText}>No Events Planned</Text>
            </View>
        );
    }

    renderItem = ({ item }) => {
        if (_.isEmpty(item)) {
            return this.renderEmptyItem();
        }

        return (
            <TouchableOpacity onPress={() => this.itemPressed(item)} style={styles.item} testID={testIDs.agenda.ITEM}>
                <View>
                    <Text style={styles.itemHourText}>{item.hour}</Text>
                    <Text style={styles.itemDurationText}>{item.duration}</Text>
                </View>
                <Text style={styles.itemTitleText}>{item.title}</Text>

            </TouchableOpacity>
        );
    };

    getMarkedDates = () => {
        const marked = {};
        this.state.ITEMS.forEach(item => {
            // NOTE: only mark dates with data
            if (item.data && item.data.length > 0 && !_.isEmpty(item.data[0])) {
                marked[item.title] = { marked: true };
            } else {
                marked[item.title] = { disabled: true };
            }
        });
        return marked;
    };

    render() {
        // eslint-disable-next-line no-return-assign
        return (
            <Provider>
                <CalendarProvider
                    date={this.state.ITEMS[0].title}

                    onDateChanged={this.onDateChanged}
                    onMonthChange={this.onMonthChange}
                    showTodayButton
                    disabledOpacity={0.6}

                >
                    <Portal>
                        <Modal visible={this.state.openModal} onDismiss={this.hideModal} contentContainerStyle={this.state.containerStyle}>
                            <View>
                                <Text>Note {this.state.modalData.date}</Text>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <TextInput
                                    label="Note"
                                    mode="outlined"
                                    multiline
                                    numberOfLines={5}
                                    value={this.state.modalData.note}
                                    onChangeText={text => this.setState({
                                        modalData: {
                                            note: text,
                                            date: this.state.modalData.date,
                                            id: this.state.modalData.id
                                        }
                                    })} />

                            </View>
                            <View />
                            <Dialog.Actions>
                                <Button onPress={this.editCalendar}>Edit</Button>
                                <Button onPress={this.deleteCalendar}>Delete</Button>
                                <Button onPress={this.hideModal}>Close</Button>
                            </Dialog.Actions>
                        </Modal>
                    </Portal>
                    <Portal>
                        <Modal visible={this.state.visible} onDismiss={this.hideDialog} contentContainerStyle={this.state.containerStyle}>
                            <View>
                                <Text>New Note</Text>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <TextInput
                                    label="Date"
                                    mode="outlined"
                                    disabled="true"
                                    value={this.state.pickedDate} />
                                <TextInput
                                    multiline
                                    numberOfLines={4}
                                    label="Note"
                                    mode="outlined"
                                    style={{ marginTop: 10 }} value={this.state.note}
                                    onChangeText={text => this.setState({ note: text })} />
                            </View>
                            <Dialog.Actions>
                                <Button onPress={this.saveNote}>Save</Button>
                                <Button onPress={this.hideDialog}>Close</Button>
                            </Dialog.Actions>
                        </Modal>
                    </Portal>
                    <Portal>
                        <FAB.Group
                            open={this.state.open}
                            icon={this.state.open ? 'calendar-today' : 'calendar-today'}
                            actions={[
                                { icon: 'plus', label: 'New Note', onPress: () => this.showDialog() },
                                { icon: 'bell', label: 'To Reminder', onPress: () => this.gotoReminder() }
                            ]}
                            onStateChange={this.showButton}
                            onPress={() => {
                                if (this.state.open) {
                                    this.setState({ open: false });
                                }
                            }}
                        />
                    </Portal>
                    <View style={styles.wrapperAtas}>
                        <Text style={styles.textAtas}>Agenda</Text>
                    </View>

                    {this.props.weekView ? (
                        <WeekCalendar testID={testIDs.weekCalendar.CONTAINER} firstDay={1} markedDates={this.getMarkedDates()} />
                    ) : (
                            <ExpandableCalendar
                                testID={testIDs.expandableCalendar.CONTAINER}

                                disableAllTouchEventsForDisabledDays
                                firstDay={1}
                                markedDates={this.getMarkedDates()}

                            />
                        )}
                    <AgendaList
                        sections={this.state.ITEMS}
                        extraData={this.state}
                        renderItem={this.renderItem}
                    />
                </CalendarProvider>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    calendar: {
        paddingLeft: 20,
        paddingRight: 20
    },
    section: {
        backgroundColor: lightThemeColor,
        color: 'grey',
        textTransform: 'capitalize'
    },
    item: {
        padding: 20,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        flexDirection: 'row'
    },
    itemHourText: {
        color: 'black'
    },
    itemDurationText: {
        color: 'grey',
        fontSize: 12,
        marginTop: 4,
        marginLeft: 4
    },
    itemTitleText: {
        color: 'black',
        marginLeft: 16,
        fontWeight: 'bold',
        fontSize: 16
    },
    itemButtonContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    emptyItem: {
        paddingLeft: 20,
        height: 52,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey'
    },
    emptyItemText: {
        color: 'lightgrey',
        fontSize: 14
    },
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    textAtas: {
        marginTop: 20,
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        color: '#FFFFFF'
    }
});
