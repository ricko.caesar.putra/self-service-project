/* eslint-disable no-alert */
/* eslint-disable no-console */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { Platform, StatusBar, StyleSheet, Text, View } from 'react-native';
import { colors } from '../../utils/colors';
import * as Animatable from 'react-native-animatable';
import MapView, { Callout, Circle, Marker, Polygon, PROVIDER_GOOGLE } from 'react-native-maps';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Geolocation from 'react-native-geolocation-service';
import { isPointInPolygon } from 'geolib';
import { request, PERMISSIONS } from 'react-native-permissions';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import fetchHomeProfile from '../../stores/actions/homeProfileAction';
import moment from 'moment';
import querySubmitAttendance from '../../Api/submitAttendanceApi';
import AsyncStorage from '@react-native-async-storage/async-storage';

const PresencePage = (props) => {

    const [curLat, setCurLat] = useState(-6.230628329727796);
    const [curLong, setCurLong] = useState(106.81748120928937);
    const [curAcc, setCurAcc] = useState(0);
    const [isOut, setIsOut] = useState(true);
    const data = props.homeProfileStore.payload.data[0];
    const [curTime, setCurTime] = useState(moment().format('HH:mm'));
    const [curDate, setCurDate] = useState(moment().format('dddd, MMMM Do YYYY '));
    const [curDateTime, setCurDateTime] = useState(moment().format('YYYY-MM-DD HH:mm:ss'));
    const [type, setType] = useState('clock in');
    const coordinatesPoly = data.location;

    useEffect(() => {
        checkClock();
        let secTimer = setInterval(() => {
            setCurTime(moment().format('HH:mm:ss'));
            setCurDate(moment().format('dddd, MMMM Do YYYY '));
            setCurDateTime(moment().format('YYYY-MM-DD HH:mm:ss'));
        }, 1000);
        return () => clearInterval(secTimer);
    }, []);

    useEffect(() => {
        checkClock();
    }, [props.homeProfileStore.payload.data[0]]);


    useEffect(() => {
        requestLocationPermission();
        checkIsOut();
    }, [curLat, curLong]);

    const checkClock = () => {
        if (!props.homeProfileStore.payload.data[0].attendance[0]) {
            setType('clock in');
        }
        else {
            if (moment(props.homeProfileStore.payload.data[0].attendance[0].clockin).utc().format('MM/DD/YYYY') === moment().format('L')) {
                setType('clock out');
            }
            if (moment(props.homeProfileStore.payload.data[0].attendance[0].clockout).utc().format('MM/DD/YYYY') === moment().format('L')) {
                return setType('clock in');
            }
            // else {
            //     return setType('clock in');
            // }
        }
    };

    const requestLocationPermission = async () => {
        if (Platform.OS === 'ios') {
            var response = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
            console.log('iPhone: ' + response);

            if (response === 'granted') {
                Geolocation.getCurrentPosition(
                    (position) => {
                        // console.warn(position);
                        setCurLat(position.coords.latitude);
                        setCurLong(position.coords.longitude);
                        setCurAcc(position.coords.accuracy);
                    },
                    (error) => {
                        // See error code charts below.
                        console.warn(error.code, error.message);
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                );
            }
        } else {
            var response = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
            // console.log('Android: ' + response);

            if (response === 'granted') {
                Geolocation.watchPosition(
                    (position) => {
                        // console.warn(position);
                        setCurLat(position.coords.latitude);
                        setCurLong(position.coords.longitude);
                        setCurAcc(position.coords.accuracy);
                    },
                    (error) => {
                        // See error code charts below.
                        // console.warn(error.code, error.message);
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50 }
                );
                // stopObserving()
            }
        }
    };

    const checkIsOut = () => {
        isPointInPolygon({
            latitude: curLat,
            longitude: curLong
        }, coordinatesPoly) ?
            setIsOut('in office')
            : setIsOut('out office');
    };

    const fetchSubmitAttendance = async (type, date, location, token) => {
        try {
            await querySubmitAttendance(type, date, location, token)
                .then(async (response) => {
                    // console.warn(response.data)
                    if (response.data == null) {
                        alert('submit failed');
                    } else {
                        type === 'clock in' ? setType('clock out') : setType('clock in');
                        const token = await AsyncStorage.getItem('@token');
                        props.dispatchHomeProfile(token);
                        alert(`berhasil ${type}`);
                    }
                });
        } catch (error) {
            alert('submit failed');
        }
    };

    return (
        <View style={{ backgroundColor: '#333333' , flex: 1 }}>
            <StatusBar style={{ marginTop: 20 }} barStyle="dark-content" translucent backgroundColor="rgba(0,0,0,0)" />
            <Animatable.View
                style={styles.wrapperMap}
                duration={2000}
                animation="zoomIn"
                // animation="pulse"
                easing="ease-in-sine"
                // easing="ease-out"
                // iterationCount="infinite"
                direction="alternate"
            >
                <MapView
                    style={styles.map}
                    provider={PROVIDER_GOOGLE}
                    region={{
                        latitude: curLat,
                        longitude: curLong,
                        latitudeDelta: 0.008,
                        longitudeDelta: 0.008
                    }}
                >
                    <Polygon
                        coordinates={coordinatesPoly}
                        fillColor={'rgba(100,200,200,0.6)'}
                        strokeWidth={3}
                    />
                    <Circle
                        center={{ latitude: curLat, longitude: curLong }}
                        radius={curAcc}
                        fillColor={'rgba(200,100,200,0.6)'}
                    />
                    <Marker
                        draggable
                        coordinate={{
                            latitude: curLat,
                            longitude: curLong
                        }}
                        title={'rumah'}>
                        <Callout onPress={() => {
                            isPointInPolygon({
                                latitude: curLat,
                                longitude: curLong
                            }, coordinatesPoly) ?
                                alert('di dalam polygon')
                                : alert('di luar polygon');
                        }
                        }
                         />
                    </Marker>
                </MapView>
                <View style={styles.positionRefresh}>
                    <TouchableOpacity onPress={() => {
                        requestLocationPermission();
                        checkIsOut();
                    }}>
                        <Icon name={'ios-reload-circle'} size={35} color="black" />
                    </TouchableOpacity>
                </View>
                {isOut === 'out office' ?
                    <Text style={styles.positionWarning}>Are you outside of the office area?</Text>
                    : null
                }
            </Animatable.View>

            <View style={{ height: '70%' }} />
            <Animatable.View
                style={styles.wrapperBawah}
                duration={2000}
                animation="fadeInUp"
                // animation="pulse"
                easing="ease-in-sine"
                // easing="ease-out"
                // iterationCount="infinite"
                direction="alternate"
            >
                <View style={styles.wrapperTime}>
                    <Text style={styles.textTime}>{curTime}</Text>
                    <Text style={styles.textTanggal}>{curDate}</Text>
                    {data.attendance[0] ?
                        <View>
                            <Text style={styles.textClockIn}>Last Clock in : </Text>
                            <Text style={styles.textClockIn}>
                                {moment(props.homeProfileStore.payload.data[0].attendance[0].clockin).utc().format('MM/DD/YYYY') === moment().format('L') ?
                                    moment(data.attendance[0].clockin).utcOffset('+00:00').format('hh:mm:ss a')
                                    : null}
                            </Text>
                        </View>
                        : null}
                </View>

                <View style={styles.wrapperClockIn}>
                    <TouchableOpacity style={styles.wrapperClockIn} onPress={async () => {
                        // console.warn(`long: ${curLong}, lat: ${curLat}, out: ${isOut}, type:${type} ,date=${curDateTime}`)
                        const token = await AsyncStorage.getItem('@token');
                        await fetchSubmitAttendance(type, curDateTime, isOut, token);
                        props.dispatchHomeProfile(token);
                    }}>
                        <Text style={styles.textTombol}>{type}</Text>
                    </TouchableOpacity>
                </View>

            </Animatable.View>

        </View>
    );
};

function mapStateToProps(state) {
    return {
        homeProfileStore: state.homeProfileStore
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchHomeProfile: (token) => dispatch(fetchHomeProfile(token))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(PresencePage);

const styles = StyleSheet.create({
    wrapperMap: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        alignSelf: 'center',
        borderRadius: 20,
        backgroundColor: 'grey'
    },
    wrapperBawah: {
        flexDirection: 'row',
        marginTop: 10,
        width: '100%',
        height: '30%',
        alignSelf: 'center',
        alignItems: 'center',
        borderTopEndRadius: 40,
        borderTopStartRadius: 40,
        justifyContent: 'space-around',
        backgroundColor: colors.merah

    },
    textTanggal: {
        color: colors.putih,
        fontWeight: 'bold',
        fontSize: 15
    },
    textTime: {
        color: colors.putih,
        fontWeight: 'bold',
        fontSize: 24
    },
    textClockIn: {
        color: colors.putih
    },
    wrapperTime: {
        margin: 20,
        justifyContent: 'center'
    },
    wrapperClockIn: {
        width: 100,
        height: 100,
        borderRadius: 100,
        backgroundColor: colors.hitam,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textTombol: {
        alignSelf: 'center',
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white'
    },
    map: {
        height: '100%',
        width: '100%'
    },
    positionWarning: {
        position: 'absolute',
        top: 40,
        left: 10,
        backgroundColor: '#FFFFFF88',
        width: 300,
        height: 40,
        textAlignVertical: 'center',
        textAlign: 'center',
        borderRadius: 10,
        fontWeight: 'bold',
        borderWidth: 3
    },
    positionRefresh: {
        position: 'absolute',
        top: 40,
        right: 10,
        backgroundColor: '#FFFFFF88',
        width: 40,
        height: 40,
        borderRadius: 10,
        fontWeight: 'bold',
        borderWidth: 3,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
