import React, { useEffect, useState } from "react";
import { Dimensions, StyleSheet, Text, View } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import queryPodcast from "../../Api/podcastApi";
import PodcastCard from "../../components/PodcastCard";
import { colors } from "../../utils";
import AsyncStorage from '@react-native-async-storage/async-storage'
const { width, height } = Dimensions.get('screen');

export default function ListPodcastPage() {
  // const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjkwMDE2LCJpYXQiOjE2MTMzNTgyNjksImV4cCI6MTYxMzQ0NDY2OX0.Fvtt1M_E0T7WWlUtvBT9sJLxw23ZnWpnLqWgUYeGWpw'
  const [podcastData, setPodcastData] = useState([])
  
  useEffect(() => {
    fetchDataPodcast()
  }, [])

  const fetchDataPodcast = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      await queryPodcast(token)
        .then((response) => {
          // console.warn(response.data)
          if (response.data == null) {
            alert('fetch podcast failed')
          } else {
            { !response.data ? console.warn(response.code) : setPodcastData(response.data.data) }
          }
        })
    } catch (error) {
      alert('fetch news failed')
    }
  }

  // const getPodcast = async () => {
  //   const dataToken = await AsyncStorage.getItem('@token')
  //   fetchDataPodcast(dataToken)
  //   // setToken(dataToken)
  // }



  // useEffect(() => {
  //   if(token){
  //   fetchDataPodcast(token);}
  // }, [token]);



  return (
    <View style={styles.container}>
      <View style={styles.wrapperAtas}>
        <Text style={styles.textAtas}>Podcast</Text>
      </View>
      <FlatList
        data={podcastData}
        // horizontal
        pagingEnabled
        style={{ flexGrow: 0 }}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{ alignItems: 'center' }}
        snapToInterval={width}
        decelerationRate={0}
        bounces={false}
        renderItem={({ item, index }) => {
          return <PodcastCard
            index={index}
            url={item.url}
            title={item.title}
            artist={item.artist}
            artwork={item.artwork}
            duration={item.duration}
            description={item.description}
            published_date={item.published_date}
          />
        }}
        keyExtractor={(item) => item.id}
      // onEndReached={() => handleLoadMore()}
      // onEndReachedThreshold={0.3}
      />

    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  description: {
    width: "80%",
    marginTop: 20,
    textAlign: "center"
  },
  player: {
    marginTop: 40
  },
  state: {
    marginTop: 20
  },
  wrapperAtas: {
    width: '100%',
    height: 80,
    backgroundColor: colors.merah,
    justifyContent: 'center',
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20,
  },
  textAtas: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center'

  }
});
