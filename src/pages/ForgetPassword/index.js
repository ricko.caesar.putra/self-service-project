import React, {useState, useEffect} from 'react'
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native'
import Input from '../../components/atoms/Input'
import { colors } from '../../utils'
import doResetPassword from '../../Api/forgetPasswordApi'


const ForgetPassword = (props) => {
    const [emailAddress, setEmailAddress] = useState('')


    const onPressedReset = async (email) => {
        try {
            await doResetPassword(email)
                .then((response) => {
                    if (response.data.code == 200) {
                        alert('Password is reset successfully!')
                        props.navigation.replace('Login')
                    } else{
                        alert('Reset Password fails')
                    }
                })
        } catch (error) {
            throw error
        }
    }
    
    return (
        <View
            style={styles.container}>
            <Text 
                style={styles.disclaimerText}>
                    Your password will be reset and we will send the new password to your e-mail address.
            </Text>
            
            <View style={styles.formContainer}>
                <Input
                    type={'email'}
                    placeholder={'E-mail Address'}
                    value={emailAddress}
                    onChangeText={(value)=> setEmailAddress(value)}
                />
            </View>

            <TouchableOpacity
                style={styles.forgetButton}
                onPress={()=> onPressedReset(emailAddress)}>
                    <Text style={{color: colors.putih}}>
                        RESET PASSWORD
                    </Text>
            </TouchableOpacity>
            

        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: colors.merah,
        justifyContent: 'center',
    },
    disclaimerText:{
        marginHorizontal: 20,
        textAlign:'justify',
        color: colors.putih,
    },
    formContainer:{
        marginTop: 20,
        marginHorizontal : 20,
        padding: 5,
        borderRadius: 30,
        backgroundColor: colors.putih,
    },
    forgetButton:{
        marginTop: 40,
        height: 50,
        borderRadius: 25,
        backgroundColor: colors.hitam,
        marginHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
    }
})

export default ForgetPassword