/* eslint-disable no-alert */
import React, { useState, useCallback, useEffect } from 'react';
import {
    Dimensions, StyleSheet, Text, View,
    RefreshControl
} from 'react-native';
import HistoryAttendanceCard from '../../components/HistoryAttendanceCard';
import queryHistoryAttendance from '../../Api/historyAttendanceApi';
import { FlatList } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { colors } from '../../utils';
const { width, height } = Dimensions.get('screen');

export default function AttendanceHistory() {
    const [refreshing, setRefreshing] = useState(false);
    const [historyData, setHistoryData] = useState([]);

    useEffect(() => {
        fetchDataNews();
    }, []);

    const onRefresh = useCallback(async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            setRefreshing(true);
            await fetchDataNews(token);
            setRefreshing(false);
        }
        catch (error) {
            alert(error);
        }

    }, []);

    const fetchDataNews = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            await queryHistoryAttendance(token)
                .then((response) => {
                    if (response.data == null) {
                        alert('fetch history failed');
                    } else {
                        !response.data ? console.warn(response.code) : setHistoryData(response.data.data);
                    }
                });
        } catch (error) {
            alert(error);
        }
    };

    return (
        <View style={{ backgroundColor: '#C4C4C4', height: height }}>
            <View style={styles.wrapperAtas}>
                <Text style={styles.textAtas}>Attendance History</Text>
            </View>
            <FlatList
                contentContainerStyle={styles.scrollView}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }
                data={historyData}
                // horizontal
                pagingEnabled
                style={{ flexGrow: 0 }}
                showsHorizontalScrollIndicator={false}
                // contentContainerStyle={{ alignItems: 'center' }}
                snapToInterval={width}
                decelerationRate={0}
                bounces={false}
                renderItem={({ item, index }) => {
                    return <HistoryAttendanceCard
                        index={index}
                        data={item}
                        date={item.date}
                        duration={item.duration}
                        clockin={item.clockin}
                        clockout={item.clockout}
                    />;
                }}
                keyExtractor={(item) => item.id.toString()}
            />

        </View>
    );
}

const styles = StyleSheet.create({
    scrollView: {
        paddingHorizontal: 10,
        backgroundColor: '#C4C4C4',
        paddingBottom: 100
    },
    userCard: {
        backgroundColor: 'white',
        paddingVertical: 6,
        paddingHorizontal: 6,
        borderRadius: 10,
        marginTop: 10,
        display: 'flex',
        // flexDirection: 'row',
        alignItems: 'center',
        height: 130,
        width: '100%'
    },
    userImage: {
        width: 40,
        height: 40,
        borderRadius: 100
    },
    userCardRight: {
        paddingHorizontal: 10
    },
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    textAtas: {
        marginTop: 20,
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        color: '#FFFFFF'
    }
});
