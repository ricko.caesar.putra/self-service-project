/* eslint-disable no-alert */
import React, { useState, useCallback, useEffect } from 'react';
import { Dimensions, Image, StatusBar, StyleSheet, Text, View, RefreshControl } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { colors } from '../../utils/colors';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import * as Animatable from 'react-native-animatable';
import { podcastPng, roomPng, attendancePng, leavePng, calendarPng, medicalPng } from '../../assets';
import Option from '../../components/atoms/Option';
import NewsCard from '../../components/NewsCard';
import PasswordCard from '../../components/PasswordCard';
import { WebView } from 'react-native-webview';
import queryNews from '../../Api/newsApi';
import checkPasswordExpiry from '../../Api/checkPasswordExpiryApi';
import { connect } from 'react-redux';
import fetchHomeProfile from '../../stores/actions/homeProfileAction';
import fetchUserProfile from '../../stores/actions/userAction';
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
const { width } = Dimensions.get('screen');

const HomePage = (props) => {
    const avatarIcon = props.userStore.payload.avatar ? props.userStore.payload.avatar : Image.resolveAssetSource(require('../../assets/images/avatar-user.png')).uri;
    const [refreshing, setRefreshing] = useState(false);
    const [users, setUsers] = useState({});
    const [content, setContent] = useState('Pop News');
    const [newsData, setNewsData] = useState([]);
    const [isClockIn, setIsClockIn] = useState(false);
    const [isClockOut, setIsClockOut] = useState(false);
    const [isExpired, setIsExpired] = useState(false);
    const [imageHeadersState, setImageHeadersState] = useState({});

    useEffect(() => {
        getData();
        setImageHeaders();
    }, []);

    const getData = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            await fetchDataNews(token);
            await props.dispatchHomeProfile(token);
            await props.dispatchUserProfile(token);
            // checkClock();
        }
        catch (error) {
            alert(error);
        }
    };

    useEffect(() => {
        checkIsExpired();
    }, [isExpired]);

    const checkIsExpired = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            await checkPasswordExpiry(token)
                .then((response) => {
                    if (response.status === 200) {
                        setIsExpired(true);
                    } else {
                        setIsExpired(false);
                    }
                });
        } catch (error) {
            alert('check expired :', error);
        }
    };

    useEffect(() => {
        if (props.homeProfileStore.payload.data) {
            setUsers(props.homeProfileStore.payload.data[0]);
            checkClock();
        }
    }, [props.homeProfileStore.payload.data]);

    const onRefresh = useCallback(async () => {
        try {
            setRefreshing(true);
            await getData();
            setRefreshing(false);
        }
        catch (error) {
            alert(error);
        }
    }, []);

    const checkClock = () => {
        if (!props.homeProfileStore.payload.data[0].attendance[0]) {
            setIsClockIn(false);
            setIsClockOut(false);
        }
        else {
            // console.warn(`data users`, users)
            if (props.homeProfileStore.payload.data[0].attendance[0]) {
                if (moment(props.homeProfileStore.payload.data[0].attendance[0].clockin).utc().format('MM/DD/YYYY') === moment().format('L')) {
                    setIsClockIn(true);
                }
                if (moment(props.homeProfileStore.payload.data[0].attendance[0].clockout).utc().format('MM/DD/YYYY') === moment().format('L')) {
                    return setIsClockOut(true);
                }
            }
            else {
                
                setIsClockIn(false);
                setIsClockOut(false);
            }
        }
    };

    const fetchDataNews = async (token) => {
        try {
            await queryNews(token)
                .then((response) => {
                    // console.warn(response.data)
                    if (response.data == null) {
                        alert('fetch news failed');
                    } else {
                        !response.data ? console.warn(response.code) : setNewsData(response.data.data);
                    }
                });
        } catch (error) {
            alert('fetch news failed');
        }
    };

    const setImageHeaders = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');

            const headers = {
                'api_key': `76f8a1fab09bc13f2e48be45689dd074`,
                'Authorization': `Bearer ${token}`
            };

            setImageHeadersState(headers);
        } catch (error) {
            throw error;
        }
    };

    return (
        <View style={{ flex: 1 }}>
            <StatusBar barStyle="dark-content" translucent backgroundColor="rgba(0,0,0,0)" />
            <Animatable.View
                style={styles.wrapperHeader}
                duration={2000}
                animation="fadeInDown"
                easing="ease-in-sine"
                direction="alternate"
            >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <View style={{ flexDirection: 'row' , marginHorizontal: 20, alignItems: 'center'}}>
                        <Image
                            source={{
                                uri: avatarIcon,
                                headers: imageHeadersState
                            }}
                            width={1}
                            height={1}
                            style={styles.avatar}
                        />
                        <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                            <Text style={styles.textGreeting}>Hi,</Text>
                            <Text style={styles.textName}>{users.username}</Text>
                            <Text style={styles.textClockIn}>
                                {isClockIn ?
                                    `Last Clock in : ${moment(users.attendance[0].clockin).utcOffset('+00:00').format('hh:mm:ss a')}`
                                    : 'you haven\'t clocked in yet'}
                            </Text>
                            {/* <Text style={styles.textClockIn}>
                                {isClockIn ? `You've been working ${moment(users.attendance[0].clockin).startOf("hour").fromNow()}` : null}
                            </Text> */}
                            <Text style={styles.textClockIn}>
                                {isClockIn && isClockOut ? 'you\'ve already clock out' : null}
                            </Text>
                        </View>
                        
                    </View>
                    <Icon name={'ios-notifications'} style={{marginHorizontal: 20,}} size={30} color='white' />
                </View>
                {/* <View style={styles.floatingInfo} /> */}
            </Animatable.View>

            <Animatable.View
                style={styles.wrapperOpsi}
                duration={2000}
                animation="zoomIn"
                easing="ease-in-sine"
                direction="alternate"
            >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                    <TouchableOpacity onPress={() => props.navigation.navigate('AttendanceHistory')}>
                        <View style={styles.contentOpsi}>
                            {/* <LottieView source={attendanceIcon} style={{ width: 45, height: 45, }} autoPlay loop /> */}
                            <Image
                                source={attendancePng}
                                style={{ width: 50, height: 50 }}
                            />
                            
                        </View>
                        <Text style={styles.textContent}>Attendance History</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('MedicalBenefitPage')}>
                        <View style={styles.contentOpsi}>
                            <Image
                                source={medicalPng}
                                style={{ width: 50, height: 50 }}
                            />
                            {/* <LottieView source={medicalIcon} style={{ width: 45, height: 45, }} autoPlay loop /> */}
                            
                        </View>
                        <Text style={styles.textContent}>Medical Benefit</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('CalendarPage')}>
                        <View style={styles.contentOpsi}>
                            <Image
                                source={calendarPng}
                                style={{ width: 50, height: 50 }}
                            />
                            {/* <LottieView source={calendarIcon} style={{ width: 45, height: 45, }} autoPlay loop /> */}
                            
                        </View>
                        <Text style={styles.textContent}>Calendar</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('LeavePage')}>
                        <View style={styles.contentOpsi}>
                            <Image
                                source={leavePng}
                                style={{ width: 50, height: 50 }}
                            />
                            {/* <LottieView source={leaveIcon} style={{ width: 45, height: 45, }} autoPlay loop /> */}
                        </View>
                        <Text style={styles.textContent}>Leave</Text>
                    </TouchableOpacity>
                    {/*<TouchableOpacity onPress={() => props.navigation.navigate('PodcastStack', { screen: 'ListPodcastPage' })}>*/}
                    <TouchableOpacity onPress={() => props.navigation.navigate('MainRoomPage')}>
                        <View style={styles.contentOpsi}>
                            <Image
                                source={roomPng}
                                style={{ width: 50, height: 50 }}
                            />
                            {/* <LottieView source={otherIcon} style={{ width: 45, height: 45, }} autoPlay loop /> */}
                        </View>
                        <Text style={styles.textContent}>Room</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => props.navigation.navigate('PodcastPage')}>
                        <View style={styles.contentOpsi}>
                            <Image
                                source={podcastPng}
                                style={{ width: 50, height: 50 }}
                            />
                        </View>
                        <Text style={styles.textContent}>Podcast</Text>
                    </TouchableOpacity>
                </View>
            </Animatable.View>
            {/* <MenuUtama/> */}

            <Animatable.View
                style={styles.wrapperScroll}
                duration={2000}
                animation="fadeInUp"
                easing="ease-in-sine"
                direction="alternate"
            >
                <Option
                    options={['Pop News', 'Talks@Telkomsel']}
                    onChange={(option) => {
                        setContent(option);
                    }}
                />
                {content == 'Pop News' ?
                    <View>
                        <ScrollView
                            refreshControl={
                                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                            }
                            style={{ flexGrow: 0, marginTop:10 }}
                            contentContainerStyle={{ alignItems: 'center' }}
                            decelerationRate={0}
                            bounces={false}
                        >
                            {newsData.map((item, index) => {
                                return <NewsCard
                                    key={index}
                                    title={item.title}
                                    source={item.source_name}
                                    description={item.description}
                                    url={item.url}
                                    imageMC={item.urlToImage}
                                    publishedAt={item.publishedAt}
                                />;
                            })}
                            <View style={{height: 200,}}></View>
                        </ScrollView>
                    </View>
                    : null
                }
                {content == 'Talks@Telkomsel' ?
                    <View style={styles.cardContent}>
                        <WebView
                            style={{ marginTop: -47, alignSelf: 'center' }}
                            // height={10}
                            width={320}
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            source={{ uri: 'https://youtu.be/cDvxZ0Iw6Zs' }}
                            originWhitelist={['*']}
                            // directionalLockEnabled={true}
                            bounces={false}
                            scrollEnabled={false}
                        // overScrollMode={'content'}
                        />
                    </View>
                    : null
                }


                <View style={{ height: 550 }} />
            </Animatable.View>

            {isExpired ?
                <PasswordCard
                    backPress={() => {
                        setIsExpired(false);
                    }} /> : null
            }
        </View>
    );
};

function mapStateToProps(state) {
    return {
        homeProfileStore: state.homeProfileStore,
        userStore: state.userStore
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchHomeProfile: (token) => dispatch(fetchHomeProfile(token)),
        dispatchUserProfile: (token) => dispatch(fetchUserProfile(token))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);

const styles = StyleSheet.create({
    wrapperHeader: {
        paddingTop: '15%',
        paddingHorizontal: '3%',
        height: 250,
        width: '100%',
        backgroundColor: colors.merah,
        borderBottomStartRadius: 30,
        borderBottomEndRadius: 30,
        borderTopColor: colors.merah
    },
    avatar: {
        height: 80,
        width: 80,
        borderRadius: 100
    },
    textGreeting: {
        color: 'white',
        fontSize: 15
    },
    textName: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18
    },
    textClockIn: {
        color: 'white',
        fontSize: 12
    },
    floatingInfo: {
        width: '95%',
        height: 40,
        borderRadius: 10,
        backgroundColor: 'white',
        alignSelf: 'center',
        marginTop: 15
    },
    wrapperOpsi: {
        padding: 10,
        width: '90%',
        marginHorizontal: 20,
        //height: 220,
        backgroundColor: '#8d8d8d65',
        marginTop: -80,
        alignSelf: 'center',
        borderRadius: 20,
        justifyContent: 'space-between'
        // elevation: 10,
    },
    contentOpsi: {
        width: 80,
        height: 80,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    wrapperScroll: {
        marginTop: 10,
        backgroundColor: colors.merah,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingTop: 20

    },
    textJudulBody: {
        color: 'white',
        fontSize: 12,
        fontWeight: 'bold',
        alignSelf: 'center',
        borderWidth: 5,
        backgroundColor: colors.merah,
        borderColor: colors.hitam,
        width: 130,
        textAlign: 'center',
        textAlignVertical: 'center',
        height: 40,
        borderRadius: 20

    },
    cardContent: {
        width: '83%',
        height: 190,
        borderColor: 'black',
        borderWidth: 5,
        margin: 10,
        alignSelf: 'center'
    },
    textContent: {
        marginTop: 5,
        fontSize: 10,
        width: 80,
        color: colors.hitam,
        textAlign: 'center',
        fontWeight: 'bold'
    }
});
