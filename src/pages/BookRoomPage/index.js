import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { colors } from '../../utils';
const BookRoomPage = (props) => {

    return (
        <View>
            <View style={styles.wrapperAtas}>
                <Text style={styles.textAtas}>{props.route.params.name}</Text>
            </View>
        </View>
    );
};

export default BookRoomPage;

const styles = StyleSheet.create({
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    textAtas: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'
    }
});
