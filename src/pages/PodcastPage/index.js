import React,{useEffect,useState} from 'react'
import {ScrollView,View,Text,StyleSheet,TouchableOpacity,Image} from 'react-native'
import {colors} from '../../utils/colors/index'
import queryPodcast from '../../Api/podcastApi'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Icon from 'react-native-vector-icons/Ionicons';
import SoundPlayer from 'react-native-sound-player'


const PodcastPage = (props) => {
    const [podcastList, setPodcastList] = useState(null)
    const [selectedPodcast, setSelectedPodcast] = useState(2)
    const [isPlayed, setIsPlayed] = useState(false)
    const [imagePlayed, setImagePlayed] = useState(Image.resolveAssetSource(require('../../assets/images/podcastplayed.png')).uri)

    /*----- Variables for Playing Podcast -----*/
    //const _onFinishedLoadingURLSubscription = null

    useEffect(()=>{
        getPodcast().then((response)=>{
            setPodcastList(response)
        })

        return ()=>{
            SoundPlayer.stop()
        }
    },[])
    
    const playPodcast = () =>{
        try {
            SoundPlayer.playUrl(podcastList[selectedPodcast].url)
        } catch (error) {
            throw error
        }
    }

    const previousPodcastPressed = () => {
        SoundPlayer.stop()

        if(selectedPodcast == 0) {
            setSelectedPodcast(podcastList.length-1)
        } else {
            setSelectedPodcast(selectedPodcast-1)
        }

        setIsPlayed(true)

        playPodcast()
    }

    const playPodcastPressed = () => {
        setIsPlayed(!isPlayed)

        if(!isPlayed){
            SoundPlayer.resume()
        } else {
            SoundPlayer.pause()
        }
    }

    const nextPodcastPressed = () => {
        SoundPlayer.stop()
        if(selectedPodcast == podcastList.length-1) {
            setSelectedPodcast(0)
        } else {
            setSelectedPodcast(selectedPodcast+1)
        }

        setIsPlayed(true)

        playPodcast()
    }

    const selectingPodcast = (select) => {
        SoundPlayer.stop()

        setSelectedPodcast(select)

        setIsPlayed(true)

        playPodcast()
    }
    
    const getPodcast = async() => {
        try {
            const token = await AsyncStorage.getItem('@token')

            const result = await queryPodcast(token)


            return (result.data.data)

        } catch (error) {
            throw error
        }
    }

    return (
        <View style={{flex:1}}>
            <View style={styles.playerContainer}>
                <Image
                    style={styles.imagePlayed}
                    source={{uri: /*podcastList[selectedPodcast].artwork ? podcastList[selectedPodcast].image :*/ imagePlayed}}/>
                <View style={styles.playerButtonContainer}>
                    <TouchableOpacity
                        onPress={
                            ()=> previousPodcastPressed()
                        }>
                        <Icon
                            name={'ios-play-back-sharp'}
                            size={40}
                            color={colors.putih}/> 
                    </TouchableOpacity>
                    
                    <TouchableOpacity style={{marginHorizontal: 60,}}
                        onPress={()=> playPodcastPressed()}>
                        <Icon 
                            name={isPlayed ? 'pause' : 'play'}
                            size={40} 
                            style={styles.podcastIcon}
                            color={colors.putih}/>
                    </TouchableOpacity>
                    
                    <TouchableOpacity
                        onPress={
                            ()=> nextPodcastPressed()
                        }>
                        <Icon
                            name={'play-forward-sharp'}
                            size={40}
                            style={styles.podcastIcon}
                            color={colors.putih} />
                    </TouchableOpacity>
                </View>
            </View>

            <View style={styles.podcastListContainer}>
                <Text style={styles.podCastHeader}>Podcast List</Text>
                <ScrollView>
                    {podcastList && podcastList.map((item,i)=>{
                        return(
                            <TouchableOpacity
                                key={i}
                                onPress={()=>
                                    selectingPodcast(i)
                                }
                                style={{
                                    marginTop: 20,
                                    paddingTop: 5,
                                    paddingBottom: 5,
                                    borderRadius: 20,
                                    borderWidth: 1,
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    backgroundColor: i == selectedPodcast ? colors.merah : null,
                                    borderColor: (i == selectedPodcast)? colors.merah:colors.abuabutua}}>
                                <Image
                                    source={{uri: imagePlayed}}
                                    style={styles.cardImage}/>
                                <View style={styles.cardInfo}>
                                    <Text 
                                        style={{
                                            fontWeight: 'bold',
                                            color: i == selectedPodcast ? colors.putih : colors.hitam,
                                    }}>{item.title}</Text>
                                    <Text
                                        style={{
                                            color: i == selectedPodcast ? colors.putih : colors.hitam,
                                    }}>{item.artist}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    })}
                </ScrollView>
                
            </View>

            
        </View>

        
    )
}

export default PodcastPage

const styles = StyleSheet.create({
    playerContainer:{
        flex:1,
        backgroundColor: colors.merah,
        borderBottomLeftRadius: 40,
        borderBottomRightRadius: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imagePlayed:{
        alignItems: 'center',
        justifyContent: 'center',
        width: 200,
        height: 200,
        borderRadius: 20,
    },
    playerButtonContainer:{
        marginTop: 80,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    podcastIcon:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    podcastListContainer:{
        flex: 1,
        marginHorizontal: 20,
    },
    podCastHeader:{
        fontSize: 16,
        paddingTop: 20,
        fontWeight: 'bold',
    },
    podcastCard:{
        marginTop: 20,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 20,
        borderWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    cardImage:{
        marginLeft: 20,
        borderRadius: 10,
        width: 40,
        height: 40,
    },
    cardInfo:{
        marginLeft: 10,
    }
})