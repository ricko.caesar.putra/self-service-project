/* eslint-disable no-alert */
import React, { useState } from 'react';
import { Dimensions, StyleSheet, Text, View, Platform } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { colors } from '../../utils';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Picker } from '@react-native-picker/picker';
import { Button, CheckBoxComp } from '../../components/atoms';
import queryBookMeetingRoom from '../../Api/bookMeetingRoomApi';
import AsyncStorage from '@react-native-async-storage/async-storage';

const { width } = Dimensions.get('screen');

const BookingRoomPage = (props) => {
    const [description, setDescription] = useState(' ');
    const [date, setDate] = useState(moment(new Date()).format('YYYY-MM-DD'));
    const [pickTime, setPickTime] = useState('08:00:00');
    const [mode, setMode] = useState('');
    const [show, setShow] = useState(false);
    const [isProjector, setIsProjector] = useState(false);
    const [isHDMI, setIsHDMI] = useState(false);
    const [isBoard, setIsBoard] = useState(false);
    const [isConsumption, setIsConsumption] = useState(false);
    const [consumption, setConsumption] = useState(0);

    const pickDate = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(moment(currentDate).format('YYYY-MM-DD'));
        // console.warn(moment(currentDate).format('YYYY-MM-DD'));
    };

    const sendBook = async (room_id, time, description, is_projector, is_hdmi, is_board, is_consumption, consumption) => {
        try {
            const token = await AsyncStorage.getItem('@token');
            await queryBookMeetingRoom(token, room_id, time, description, is_projector, is_hdmi, is_board, is_consumption, consumption)
                .then(async (response) => {
                    // console.warn(response.data)
                    if (response.data == null) {
                        alert('submit failed');
                    } else {
                        alert('submit berhasil');
                    }
                });
        } catch (error) {
            alert('submit failed');
        }
    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
        // console.log(date);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    return (
        // <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View>
            <View style={styles.wrapperAtas}>
                <Text style={styles.textAtas}>{props.route.params.name}</Text>
            </View>
            <View style={{ alignItems: 'center', alignSelf: 'center', margin: 10 }}>
                <Text style={{}}>Description</Text>
                <View style={styles.input2}>
                    <TextInput
                        // style={styles.input2}
                        onChangeText={(value) => setDescription(value)}
                        value={description}
                        placeholder={'Write your needs'}
                        placeholderTextColor="#979797"
                        textAlignVertical="top"
                        multiline={true}
                        onSubmitEditing={() => {
                            null;
                        }}
                    />
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{}}>Date</Text>
                        <View style={styles.inputDate}>
                            <Text style={{ marginLeft: 8 }} onPress={showDatepicker}>{moment(date).format('YYYY-MM-DD')}</Text>
                            <Icon style={{ marginRight: 5 }} name="calendar" onPress={showDatepicker} size={25} color={'#696969'} />
                        </View>
                        <Text style={{}}>Time</Text>
                        <View style={styles.inputTime}>
                            <Picker
                                selectedValue={pickTime}
                                style={{ fontSize: 12, height: '100%', width: '100%' }}
                                textStyle={{ fontSize: 10, color: 'yellow' }}
                                onValueChange={(itemValue) => {
                                    setPickTime(itemValue);
                                }
                                }
                                itemStyle={{ fontSize: 12 }}
                            >
                                <Picker.Item label="08-09" value={'08:00:00'} />
                                <Picker.Item label="09-10" value={'09:00:00'} />
                                <Picker.Item label="10-11" value={'10:00:00'} />
                                <Picker.Item label="11-12" value={'11:00:00'} />
                                <Picker.Item label="12-13" value={'12:00:00'} />
                                <Picker.Item label="13-14" value={'13:00:00'} />
                                <Picker.Item label="14-15" value={'14:00:00'} />
                                <Picker.Item label="15-16" value={'15:00:00'} />
                                <Picker.Item label="16-17" value={'16:00:00'} />
                            </Picker>
                        </View>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{}}>Facility</Text>
                        <View style={styles.inputFasilitas}>
                            <CheckBoxComp
                                onPress={() => { setIsProjector(!isProjector); }}
                                checked={isProjector}
                                text="Projector"
                            />
                            <CheckBoxComp
                                onPress={() => { setIsHDMI(!isHDMI); }}
                                checked={isHDMI}
                                text="HDMI Cable"
                            />
                            <CheckBoxComp
                                onPress={() => { setIsBoard(!isBoard); }}
                                checked={isBoard}
                                text="Board"
                            />
                            <View style={{ flexDirection: 'row' }}>
                                <CheckBoxComp
                                    onPress={() => { setIsConsumption(!isConsumption); }}
                                    checked={isConsumption}
                                    text="Consumption"
                                />
                                <TextInput
                                    style={{ backgroundColor: 'white', height: 40, marginLeft: 2 }}
                                    onChangeText={(value) => setConsumption(value)}
                                    value={consumption}
                                    placeholder={'pax'}
                                    placeholderTextColor="#979797"
                                    textAlignVertical="top"
                                    multiline={true}
                                    onSubmitEditing={() => {
                                        null;
                                    }}
                                />
                            </View>
                        </View>
                    </View>
                </View>
            </View>

            <Button
                title={'book room'}
                onPress={() => {
                    const room_id = props.route.params.id;
                    const time = `${date} ${pickTime}`;
                    sendBook(room_id,time,description,isProjector,isHDMI,isBoard,isConsumption,consumption);
                }}
                type={'default'}
                white={false}
            />


            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={new Date()}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={pickDate}
                />
            )}
        </View>
    );
};

export default BookingRoomPage;

const styles = StyleSheet.create({
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    textAtas: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'
    },
    input2: {
        paddingHorizontal: 10,
        margin: 14,
        width: width * 0.9,
        height: 60,
        borderRadius: 10,
        backgroundColor: '#FFFFFF'
    },
    inputDate: {
        marginVertical: 14,
        width: width * 0.4,
        paddingHorizontal: 10,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    inputTime: {
        margin: 14,
        width: width * 0.4,
        paddingHorizontal: 15,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'center'
    },
    inputFasilitas: {
        width: width * 0.5,
        marginVertical: 14,
        marginRight: 10,
        padding: 5,
        height: 180,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        alignItems: 'flex-start'
    }

});
